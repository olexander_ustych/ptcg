//#include <GL/freeglut.h>
//#include <GL/gl.h>
//
//GLfloat knots[] = { 0, 0, 0, 1, 2, 3, 4, 4, 4 };
//GLfloat control_points[][3] = {
//    {-1, 0, 0},
//    {-1, 1, 0},
//    {0, -1, 0},
//    {0, 1, 0},
//    {1, -1, 0},
//    {1, 0, 0},
//    {1, 1, 0}
//};
//
//void display() {
//    glClear(GL_COLOR_BUFFER_BIT);
//
//    glColor3f(1.0, 1.0, 1.0);
//
//     //Define the B-spline curve
//    glMap1f(GL_MAP1_VERTEX_3, 0, 1, 3, 7, &control_points[0][0]);
//    glMap1f(GL_MAP1_INDEX, 0, 1, 0, 9, knots);
//    glEnable(GL_MAP1_VERTEX_3);
//
//     //Draw the B-spline curve
//    glBegin(GL_LINE_STRIP);
//    for (int i = 0; i <= 30; i++)
//        glEvalCoord1f((GLfloat)i / 30.0);
//    glEnd();
//
//    glutSwapBuffers();
//}
//
//void init() {
//    glClearColor(0.0, 0.0, 0.0, 1.0);
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluOrtho2D(-2, 2, -2, 2);
//    glMatrixMode(GL_MODELVIEW);
//}
//
//int main(int argc, char** argv) {
//    glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
//    glutInitWindowSize(500, 500);
//    glutCreateWindow("B-spline Curve");
//    init();
//    glutDisplayFunc(display);
//    glutMainLoop();
//    return 0;
//}