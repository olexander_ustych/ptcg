//#include <iostream>
//#include <GL/freeglut.h>
//#include <cstdlib>
//using namespace std;
//
//#pragma region ZPKG_KR
//
//void drawString(float x, float y, float z, char* string) {
//	glRasterPos3f(x, y, z);
//
//	for (char* c = string; *c != '\0'; c++) {
//		glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *c);
//	}
//}
//
//float angle = 0.0f;
//float lx = 0.0f, ly = 0.0f, lz = -1.0f;
//float x = 0.0f, y = 1.0f, z = 5.0f;
//int window_width = 800;
//int window_height = 600;
//
//void display() {
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//
//	//задаю ортографічне відображення, щоб було гарно видно прямокутник і його перетворення
//	glOrtho(-20.0, 20.0, -20.0, 20.0, -20.0, 20.0);
//
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//
//	glPushMatrix();
//	//зменшуємо
//	glScalef(-10.0f, -2.0f, -1.5f);
//
//	//обертаємо навколо OZ
//	glRotatef(30, 0.0f, 0.0f, 1.0f);
//
//	//ескізний шрифт
//
//	glRasterPos2i(120, 420);
//	char s[] = "Rectangle";
//	//drawString(0.5f, 0.5f, 1.0f, s);
//
//	//щоб можна було інтерполяційно зафарбувати
//	glShadeModel(GL_SMOOTH);
//
//	glBegin(GL_QUADS);
//		glColor3f(0.0, 191.0, 255.0);
//		glVertex3f(1.0, -2.0, 3.0);
//
//		glColor3f(249.0, 132.0, 229.0);
//		glVertex3f(4.0, -2.0, 3.0);
//
//		glColor3f(1.0f, 1.0f, 0.0);
//		glVertex3f(4.0, 5.0, 3.0);
//
//		glColor3f(1.0f, 0.0, 0.0);
//		glVertex3f(1.0, 5.0, 3.0);
//	glEnd();
//	glPopMatrix();
//
//	glutSwapBuffers();
//}
//
//void reshape(int width, int height) {
//	if (height == 0) {
//		height = 1;
//	}
//	window_width = width;
//	window_height = height;
//	glViewport(0, 0, window_width, window_height);
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(45.0f, (GLfloat)window_width / (GLfloat)window_height, 0.1f, 100.0f);
//}
//#pragma endregion
//
//int main(int argc, char** argv)
//{
//	// window init 
//	int winPosX, winPosY, winSizeX, winSizeY;
//	winPosX = 300;
//	winPosY = 80;
//	winSizeX = 500;
//	winSizeY = 500;
//	//// string winName = "KR-ZPKG";
//
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//	//glutInitWindowSize(window_width, window_height);
//	glutInitWindowPosition(winPosX, winPosY);
//	glutInitWindowSize(winSizeX, winSizeY);
//	glutCreateWindow("ZPKG Control Work");
//	glEnable(GL_DEPTH_TEST);
//	glutDisplayFunc(display);
//	glutReshapeFunc(reshape);
//	glutMainLoop();
//	return 0;
//}