//#include <chrono>
//#include <sstream>
//#include <string>
//#include <GL/glut.h>
//#include "StopwatchValues.h";
//
//using namespace std::chrono;
//
//auto currentFont = GLUT_BITMAP_TIMES_ROMAN_24;
//auto speed = const_cast<char*>("Speed: 1x");
//
//milliseconds startTimeMs;
//milliseconds realLastTimeMs;
//
//milliseconds modifiedLastTimeMs;
//milliseconds modifiedCurrentTimeMs;
//
//void (*diffModify)(milliseconds*);
//
//void drawString(float x, float y, float z, char* string) {
//	glRasterPos3f(x, y, z);
//
//	for (char* c = string; *c != '\0'; c++) {
//		glutBitmapCharacter(currentFont, *c);
//	}
//}
//
//stopwatch_values countStopwatchValues() {
//	auto currentTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
//	auto diff = currentTimeMs - realLastTimeMs;
//	if (diffModify != nullptr) {
//		diffModify(&diff); //link to diff should be passed in interested function
//	}
//	modifiedCurrentTimeMs += diff;
//
//	realLastTimeMs = currentTimeMs;
//	stopwatch_values realCurrentStopwatchValues;
//
//	realCurrentStopwatchValues.minutes = (modifiedCurrentTimeMs / 60000).count();
//	realCurrentStopwatchValues.seconds = (modifiedCurrentTimeMs / 1000 % 60).count();
//	realCurrentStopwatchValues.dseconds = (modifiedCurrentTimeMs / 100 % 10).count();
//
//	return realCurrentStopwatchValues;
//}
//
//void display() {
//	glClear(GL_COLOR_BUFFER_BIT);
//
//	auto stopwatchValues = countStopwatchValues();
//
//	std::ostringstream oss;
//	oss << stopwatchValues.minutes << " : " << stopwatchValues.seconds << " : " << stopwatchValues.dseconds;
//
//	auto values = _strdup(oss.str().c_str());
//	const int valuessWidthText = glutBitmapLength(currentFont, (const unsigned char*)values);
//	drawString(-((float)valuessWidthText) / 600.0f, 0.1, 0, values);
//
//	char* signatures = const_cast<char*>("min : sec : ds");
//	const int signaturesWidthText = glutBitmapLength(currentFont, (const unsigned char*)signatures);
//	drawString(-((float)signaturesWidthText) / 600.0f, 0, 0, signatures);
//
//	const int speedWidthText = glutBitmapLength(currentFont, (const unsigned char*)speed);
//	drawString(-((float)speedWidthText)/600.0f, -0.1, 0, speed);	
//
//	glFlush();
//}
//
//void timer(int extra)
//{
//	glutPostRedisplay();
//	glutTimerFunc(100, timer, 0);
//}
//
//void keyboard(unsigned char key, int x, int y) {
//	switch (key) {
//	case 'a':
//		diffModify = [](milliseconds* diff) { *diff /= 2; };
//		speed = const_cast<char*>("Speed: 0.5x");
//		break;
//	case 'd':
//		diffModify = [](milliseconds* diff) { *diff *= 2; };
//		speed = const_cast<char*>("Speed: 2x");
//		break;
//	case 's':
//		diffModify = nullptr;
//		speed = const_cast<char*>("Speed: 1x");
//		break;
//	}
//}
//
//void mouse(int button, int state, int x, int y) {
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
//		glutPostRedisplay();
//	}
//}
//
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
//	glutInitWindowSize(600, 600);
//	glutCreateWindow("Stopwatch");
//
//	startTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
//	realLastTimeMs = startTimeMs;
//
//	glutDisplayFunc(display);
//	glutTimerFunc(0, timer, 0);
//
//	glutKeyboardFunc(keyboard);
//	glutMouseFunc(mouse);
//
//	glutMainLoop();
//	return 0;
//}