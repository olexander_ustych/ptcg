//#include <chrono>
//#include <sstream>
//#include <string>
//#include <GL/glut.h>
//
//using namespace std::chrono;
//
//void* currentFont = GLUT_BITMAP_TIMES_ROMAN_24;
//milliseconds startTimeMs;
//float angle = 0.0f;
//
//void drawString(float x, float y, float z, char* string) {
//	glRasterPos3f(x, y, z);
//
//	for (char* c = string; *c != '\0'; c++) {
//		glutBitmapCharacter(currentFont, *c);
//	}
//}
//
//struct stopwatch_values {
//	long minutes;
//	long seconds;
//	long dseconds;
//};
//
//stopwatch_values countStopwatchValues() {
//	auto currentTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
//	auto diff = currentTimeMs - startTimeMs;
//	
//	stopwatch_values stopwatchValues;
//
//	stopwatchValues.minutes = (diff / 60000).count();
//	stopwatchValues.seconds = (diff / 1000 % 60).count();
//	stopwatchValues.dseconds = (diff / 100 % 10).count();
//
//	return stopwatchValues;
//}
//
//void display() {
//	glClear(GL_COLOR_BUFFER_BIT);
//	
//	auto stopwatchValues = countStopwatchValues();
//
//	std::ostringstream oss;
//	oss << stopwatchValues.minutes << " : " << stopwatchValues.seconds << " : " << stopwatchValues.dseconds;
//
//	auto values = _strdup(oss.str().c_str());
//	drawString(-.25, 0, 0, values);
//
//	char* signatures = const_cast<char*>("min : sec : ds");
//	drawString(-.25, -0.1, 0, signatures);
//
//	glFlush();
//}
//
//void timer(int extra)
//{
//	glutPostRedisplay();
//	glutTimerFunc(100, timer, 0);
//}
//
//void keyboard(unsigned char key, int x, int y) {
//	switch (key) {
//	case 27:  // ESC key
//		exit(0);
//		break;
//	}
//}
//
//void mouse(int button, int state, int x, int y) {
//	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
//		glutPostRedisplay();
//	}
//}
//
//int main(int argc, char** argv) {
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
//	glutInitWindowSize(600, 600);
//	glutCreateWindow("Stopwatch");
//
//	startTimeMs = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
//
//	glutDisplayFunc(display);
//	glutTimerFunc(0, timer, 0);
//
//	glutKeyboardFunc(keyboard);
//	glutMouseFunc(mouse);
//
//	glutMainLoop();
//	return 0;
//}