//#include <GL/freeglut.h>
//#include <GL/gl.h>
//#include <iostream>
//
//using namespace std;
//
//
//void init()
//{
//	glClearColor(0, 0, 0, 0.0);
//
//}
//
//// A(1, 2, 3), В(4, -2, 3), С(3, -5, 3), D(-4, -2, 7)
//
//void display()
//{
//    glClear(GL_COLOR_BUFFER_BIT);
//
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//
//    //перспективна проєкція, щоб можна було побачити лінії
//    gluPerspective(45.0, 1.0, 0.1, 100.0);
//
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    gluLookAt(0.0, 0.0, -15.0,
//        0.0, 0.0, 0.0,
//        0.0, 1.0, 0.0);
//
//    glPushMatrix();
//
//    //зсув
//    glTranslatef(5, 3, 7);
//
//    //обертання
//    glRotatef(45.0f, 1, 0, 0);
//
//    // AB
//    glColor3f(1.0, 1.0, 0.0);  // жовтий колір
//    glLineWidth(3.0); //товщина лінії
//    
//    glEnable(GL_LINE_STIPPLE);
//    glLineStipple(1, 0x00FF); //пунктир
//
//    glBegin(GL_LINES);
//    glVertex3f(1, 2, 3);
//    glVertex3f(4, -2, 3);
//    glEnd();
//    glDisable(GL_LINE_STIPPLE);
//
//    // CD
//    glShadeModel(GL_SMOOTH); //для інтерполяційного зафарбовування
//    
//    glLineWidth(2.0); //товщина лінії
//    glBegin(GL_LINES);
//        glColor3f(1.0, 0.0, 0.0);  // червоний колір
//        glVertex3f(3, -5, 3);
//
//        glColor3f(0.0, 1.0, 0.0);  // зелений колір
//        glVertex3f(-4, -2, 7);
//    glEnd();
//
//    // Надписи
//    glColor3f(1.0, 1.0, 1.0);  
//    glRasterPos2f(1, 2); 
//    glutBitmapString(GLUT_BITMAP_HELVETICA_12, (unsigned char*)"AB");
//
//    glRasterPos2f(-4, -2);
//    glutBitmapString(GLUT_BITMAP_HELVETICA_12, (unsigned char*)"CD");
//
//    glPopMatrix();
//
//    //каркасний гексаедр
//    glutWireCube(1.0);
//
//    glFlush();
//}
//
//int main(int argc, char** argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
//	glutInitWindowSize(800, 800);
//	glutInitWindowPosition(100, 100);
//    glutCreateWindow("Task 19");
//
//	init();
//	glutDisplayFunc(display);
//	glutMainLoop();
//	return 0;
//}