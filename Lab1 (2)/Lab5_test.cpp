//#include <GL/glut.h>
//#include <GL/glu.h>
//#include <GL/gl.h>
//#include <GL/freeglut.h>
//#include "stb_image.h"
//#include <iostream>
//
//float angle = 0.0f;
//
//GLuint textureID;
//
//GLfloat light_position[] = { 5.0, 5.0, 5.0, 1.0 };
//GLfloat light_ambient[] = { 0.9, 0.1, 0.1, 1.0 };
//GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
//
//GLfloat spotlight_position[] = { 0.0f, 0.0f, 10.0f, 1.0f };
//GLfloat spotlight_direction[] = { 0.0f, 0.0f, -1.0f };
//GLfloat spotlight_ambient[] = { 0.0f, 0.0f, 1.0f, 1.0f };
//GLfloat spotlight_diffuse[] = { 0.0f, 1.0f, 0.0f, 1.0f };
//GLfloat spotlight_cutoff = 1.0f;
//GLfloat spotlight_exponent = 10.0f;
//
//GLfloat distant_light_direction[] = { 1.0f, 0.0f, -1.0f };
//GLfloat distant_light_ambient[] = { 0.0f, 0.0f, 1.0f, 1.0f };
//GLfloat distant_light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//
//GLfloat materialAmbient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
//GLfloat materialDiffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
//GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
//GLfloat shininess[] = { 50.0f };
//
//bool fogMod = true;
//
//float housePos[] = { 0, 0, 0 };
//
//float moveVector[] = { 0.005,0.01,0.012 };
//
//float rotationSpeed = 1.0f;
//
//bool isFullScreen = false;
//
//void changeMoveVector()
//{
//    if (((housePos[0] >= 1 || housePos[0] <= -1) && !isFullScreen) || ((housePos[0] >= 2 || housePos[0] <= -2) && isFullScreen))
//    {
//        moveVector[0] = -moveVector[0];
//    }
//    if (((housePos[1] >= 1 || housePos[1] <= -1) && !isFullScreen) || ((housePos[1] >= 2 || housePos[1] <= -2) && isFullScreen))
//    {
//        moveVector[1] = -moveVector[1];
//    }
//    if (housePos[2] >= 2 || housePos[2] <= -2)
//    {
//        moveVector[2] = -moveVector[2];
//    }
//}
//
//void loadTexture(const char* filename)
//{
//    int width, height, nrChannels;
//    unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
//
//    glGenTextures(1, &textureID);
//    glBindTexture(GL_TEXTURE_2D, textureID);
//
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//    if (data)
//    {
//        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
//    }
//    else
//    {
//        std::cout << "Failed to load texture" << std::endl;
//    }
//
//    stbi_image_free(data);
//}
//
//void init(void)
//{
//    glClearColor(0.0, 0.0, 0.4, 0.0);
//    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_LIGHTING);
//    glEnable(GL_LIGHT0);
//    glEnable(GL_LIGHT1);
//    glEnable(GL_LIGHT2);
//
//    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
//    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
//
//    glLightfv(GL_LIGHT1, GL_POSITION, spotlight_position);
//    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotlight_position);
//    glLightfv(GL_LIGHT1, GL_AMBIENT, spotlight_ambient);
//    glLightfv(GL_LIGHT1, GL_DIFFUSE, spotlight_diffuse);
//    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, spotlight_cutoff);
//    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, spotlight_exponent);
//
//    glLightfv(GL_LIGHT2, GL_POSITION, distant_light_direction);
//    glLightfv(GL_LIGHT2, GL_AMBIENT, distant_light_ambient);
//    glLightfv(GL_LIGHT2, GL_DIFFUSE, distant_light_diffuse);
//
//    glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
//    glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
//    glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
//    glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
//
//    loadTexture("C:\\Users\\dimon\\Desktop\\brick.jpg");
//}
//
//void drawFog(float r, float g, float b, float fogDepth)
//{
//    glEnable(GL_FOG);
//    if (fogMod)
//    {
//        glFogf(GL_FOG_START, 1.0f);
//        glFogf(GL_FOG_END, fogDepth);
//        glFogi(GL_FOG_MODE, GL_LINEAR);
//
//    }
//    else
//    {
//        glFogf(GL_FOG_DENSITY, 0.5f);
//        glFogi(GL_FOG_MODE, GL_EXP);
//    }
//
//
//    GLfloat fogColor[4] = { r, g, b, 1.0f };
//    glFogfv(GL_FOG_COLOR, fogColor);
//    glClearColor(r, g, b, 1.0f);
//}
//
//void drawHouse(void)
//{
//    glEnable(GL_TEXTURE_2D);
//    glBindTexture(GL_TEXTURE_2D, textureID);
//    glEnable(GL_COLOR_MATERIAL);
//    glColor3f(0.5, 0.5, 0.5);
//    glBegin(GL_QUADS);
//
//    //front
//    glTexCoord2f(0.0f, 0.0f);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//
//    glTexCoord2f(1.0f, 0.0f);
//    glVertex3f(0.5f, -0.5f, -0.5f);
//
//    glTexCoord2f(1.0f, 1.0f);
//    glVertex3f(0.5f, 0.5f, -0.5f);
//
//    glTexCoord2f(0.0f, 1.0f);
//    glVertex3f(-0.5f, 0.5f, -0.5f);
//
//    //back
//    glTexCoord2f(0.0f, 0.0f);
//    glVertex3f(-0.5f, -0.5f, 0.5f);
//
//    glTexCoord2f(1.0f, 0.0f);
//    glVertex3f(0.5f, -0.5f, 0.5f);
//
//    glTexCoord2f(1.0f, 1.0f);
//    glVertex3f(0.5f, 0.5f, 0.5f);
//
//    glTexCoord2f(0.0f, 1.0f);
//    glVertex3f(-0.5f, 0.5f, 0.5f);
//
//    //bottom
//    glTexCoord2f(0.0f, 0.0f);
//    glVertex3f(-0.5f, -0.5f, 0.5f);
//
//    glTexCoord2f(1.0f, 0.0f);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//
//    glTexCoord2f(1.0f, 1.0f);
//    glVertex3f(0.5f, -0.5f, -0.5f);
//
//    glTexCoord2f(0.0f, 1.0f);
//    glVertex3f(0.5f, -0.5f, 0.5f);
//
//    //left
//    glTexCoord2f(0.0f, 0.0f);
//    glVertex3f(-0.5f, 0.5f, -0.5f);
//
//    glTexCoord2f(1.0f, 0.0f);
//    glVertex3f(-0.5f, 0.5f, 0.5f);
//
//    glTexCoord2f(1.0f, 1.0f);
//    glVertex3f(-0.5f, -0.5f, 0.5f);
//
//    glTexCoord2f(0.0f, 1.0f);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//
//    //right
//    glTexCoord2f(0.0f, 0.0f);
//    glVertex3f(0.5f, 0.5f, -0.5f);
//
//    glTexCoord2f(1.0f, 0.0f);
//    glVertex3f(0.5f, 0.5f, 0.5f);
//
//    glTexCoord2f(1.0f, 1.0f);
//    glVertex3f(0.5f, -0.5f, 0.5f);
//
//    glTexCoord2f(0.0f, 1.0f);
//    glVertex3f(0.5f, -0.5f, -0.5f);
//
//    glEnd();
//    glDisable(GL_TEXTURE_2D);
//
//
//    glColor3f(0.5, 0.0, 0.0);
//    glBegin(GL_QUADS);
//
//    glVertex3f(-0.7, 0.5, -0.7);
//    glVertex3f(-0.7, 0.5, 0.7);
//    glVertex3f(0.7, 0.5, 0.7);
//    glVertex3f(0.7, 0.5, -0.7);
//    glEnd();
//
//    glBegin(GL_TRIANGLE_FAN);
//
//    glVertex3f(0, 1.5, 0);
//
//    glVertex3f(-0.7, 0.5, -0.7);
//    glVertex3f(-0.7, 0.5, 0.7);
//
//    glVertex3f(-0.7, 0.5, 0.7);
//    glVertex3f(0.7, 0.5, 0.7);
//
//    glVertex3f(0.7, 0.5, 0.7);
//    glVertex3f(0.7, 0.5, -0.7);
//
//    glVertex3f(0.7, 0.5, -0.7);
//    glVertex3f(-0.7, 0.5, -0.7);
//
//    glEnd();
//    glDisable(GL_COLOR_MATERIAL);
//}
//
//void display(void)
//{
//    changeMoveVector();
//    angle += rotationSpeed;
//    if (angle >= 360.0f)
//    {
//        angle -= 360.0f;
//    }
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    gluLookAt(0.0, 0.0, 5.0,
//        0.0, 0.0, 0.0,
//        0.0, 1.0, 0.0);
//
//    glPushMatrix();
//    glTranslatef(housePos[0], housePos[1], housePos[2]);
//    housePos[0] += moveVector[0];
//    housePos[1] += moveVector[1];
//    housePos[2] += moveVector[2];
//    drawFog(0.2f, 0.4f, 0.5f, 8.0f);
//    glRotatef(angle, 0.5, -0.5, 0.5);
//    drawHouse();
//    glPopMatrix();
//    glutSwapBuffers();
//}
//
//void reshape(int w, int h)
//{
//    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluPerspective(40.0, (GLfloat)w / (GLfloat)h, 1.0, 100.0);
//}
//
//void draw(int a)
//{
//    glutPostRedisplay();
//    glutTimerFunc(10, draw, 0);
//}
//
//void mouse(int button, int state, int x, int y)
//{
//    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
//    {
//        fogMod = !fogMod;
//    }
//}
//
//void menuFcn(int choice)
//{
//
//    switch (choice)
//    {
//    case 1:
//        rotationSpeed -= 0.2;
//        break;
//    case 2:
//        rotationSpeed += 0.2;
//        break;
//    case 3:
//        housePos[0] = 0;
//        housePos[1] = 0;
//        housePos[2] = 0;
//        moveVector[0] /= 1.5;
//        moveVector[1] /= 1.5;
//        moveVector[2] /= 1.5;
//        break;
//
//    case 4:
//        housePos[0] = 0;
//        housePos[1] = 0;
//        housePos[2] = 0;
//        moveVector[0] *= 1.5;
//        moveVector[1] *= 1.5;
//        moveVector[2] *= 1.5;
//        break;
//    case 5:
//        if (glIsEnabled(GL_LIGHT0))
//        {
//            glDisable(GL_LIGHT0);
//        }
//        else
//        {
//            glEnable(GL_LIGHT0);
//        }
//        break;
//
//    case 6:
//        if (glIsEnabled(GL_LIGHT1))
//        {
//            glDisable(GL_LIGHT1);
//        }
//        else
//        {
//            glEnable(GL_LIGHT1);
//        }
//        break;
//    case 7:
//        if (glIsEnabled(GL_LIGHT2))
//        {
//            glDisable(GL_LIGHT2);
//        }
//        else
//        {
//            glEnable(GL_LIGHT2);
//        }
//        break;
//    case 8:
//        if (isFullScreen)
//        {
//            glutLeaveFullScreen();
//            isFullScreen = !isFullScreen;
//        }
//        else
//        {
//            glutFullScreen();
//            isFullScreen = !isFullScreen;
//        }
//        break;
//    }
//
//}
//
//int main(int argc, char** argv)
//{
//    glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
//    glutInitWindowSize(640, 640);
//    glutCreateWindow("House");
//
//    init();
//    glutCreateMenu(menuFcn);
//
//    glutAddMenuEntry("Decrease Rotation Speed", 1);
//    glutAddMenuEntry("Increase Rotation Speed", 2);
//    glutAddMenuEntry("Decrease Move Speed", 3);
//    glutAddMenuEntry("Increase Move Speed", 4);
//    glutAddMenuEntry("ON/OFF LIGHT0", 5);
//    glutAddMenuEntry("ON/OFF LIGHT1", 6);
//    glutAddMenuEntry("ON/OFF LIGHT2", 7);
//    glutAddMenuEntry("FULLSCREEN/WINDOW", 8);
//    glutAttachMenu(GLUT_RIGHT_BUTTON);
//    glutDisplayFunc(display);
//    glutReshapeFunc(reshape);
//    glutTimerFunc(10, draw, 0);
//    glutMouseFunc(mouse);
//    glutMainLoop();
//    return 0;
//}
//
