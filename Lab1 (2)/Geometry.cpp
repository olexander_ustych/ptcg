#include "Geometry.h"

Geometry::Geometry(){}


void Geometry::torus(float out_radius, float int_radius, int textureId){

	//angular step in degree
    float angle_step=5;
	//torus out. angle 
    float out_angle=0;
	//increasing value for out. angle (in radiants)
	float delta_out_angle=(angle_step/180.f)*PI;
	
	//torus int. angle
	float int_angle=0;
	//increasing value for int. angle (in radiants)
	float delta_int_angle=(angle_step/180.f)*PI;

    glEnable(GL_TEXTURE_2D); 
  	glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_MODULATE);
   
    glBindTexture(GL_TEXTURE_2D, textureId);
  
	/*Rotation angle around the center of the tube section*/
	for( int_angle=0; int_angle<=2*PI ; int_angle+=delta_int_angle ){
		
		glBegin(GL_TRIANGLE_STRIP);
		 /*Rotation angle around the center of the torus*/
		  for( out_angle=0; out_angle<=2*PI; out_angle+=delta_out_angle ){

            /*Point for (int_angle+delta_int_angle) angle*/
			Vector3 point_up = getPoint( out_angle,
				                          int_angle+delta_int_angle,
										  out_radius,
										  int_radius);
            /*Get the normal*/
			Vector3 normal_up = getPointNormal( out_angle,
				                                int_angle+delta_int_angle,
												out_radius,
												int_radius);
            /*Set the normal*/
		    glNormal3f( normal_up.x, 
				        normal_up.y, 
						normal_up.z);

            /*Set texture coordinates*/ 
            glTorusTexCoord( out_angle,
				             int_angle+delta_int_angle,
							 textureId);

			/*Set the vertex*/
			glVertex3f(  point_up.x,
				         point_up.y,
						 point_up.z);
			

            /*Point for int_angle angle*/
			Vector3 point_down = getPoint( out_angle,
				                            int_angle,
											out_radius,
											int_radius);
            /*Get the normal*/
			Vector3 normal_down = getPointNormal( out_angle,
				                                  int_angle,
												  out_radius,
												  int_radius);
		    /*Set the normal*/
			glNormal3f( normal_down.x,
				        normal_down.y, 
						normal_down.z);

		     /*Set texture coordinates*/
			glTorusTexCoord( out_angle,
				             int_angle,
							 textureId);

			/*Set the vertex*/
			glVertex3f( point_down.x,
				        point_down.y,
						point_down.z);
		
		
		 }

		glEnd();
	}

	glDisable(GL_TEXTURE_2D);
}
void Geometry::box(GLdouble width, GLdouble height, GLdouble lenght){
	 
		 // Front
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, lenght/2);
		  glVertex3d(-width/2, height/2, lenght/2);
		  glVertex3d(width/2, height/2, lenght/2);
          glVertex3d(width/2, -height/2, lenght/2);
          glVertex3d(-width/2, -height/2, lenght/2);
         glEnd();

		  // Right
		 glBegin(GL_QUADS);
		  glNormal3d(-width/2, 0, 0);
		  glVertex3d(-width/2, +height/2, -lenght/2);
		  glVertex3d(-width/2, +height/2, lenght/2);
          glVertex3d(-width/2, -height/2, lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

		  // Back
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, -lenght/2);
		  glVertex3d(width/2, height/2, -lenght/2);
		  glVertex3d(-width/2, height/2, -lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
         glEnd();

		  // Left
		 glBegin(GL_QUADS);
		  glNormal3d(width/2, 0, 0);
		  glVertex3d(width/2, height/2, lenght/2);
		  glVertex3d(width/2, height/2, -lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
          glVertex3d(width/2, -height/2, lenght/2);
         glEnd();

		 // Top
		 glBegin(GL_QUADS);
		  glNormal3d(0, height/2, 0);
		  glVertex3d(-width/2, height/2, -lenght/2);
		  glVertex3d(width/2, height/2, -lenght/2);
          glVertex3d(width/2, height/2, lenght/2);
          glVertex3d(-width/2, height/2, lenght/2);
         glEnd();

		 
		 // Bottom
		 glBegin(GL_QUADS);
		  glNormal3d(0, -height/2, 0);
		  glVertex3d(-width/2, -height/2, lenght/2);
		  glVertex3d(width/2, -height/2, lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

	
	}
void Geometry::box(GLdouble width, GLdouble height, GLdouble lenght,int textureId){
	 
	glEnable(GL_TEXTURE_2D); 
  	glShadeModel(GL_FLAT);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, textureId);
		 // Front
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, lenght/2);
		  glTexCoord2f(0, 0);
		  glVertex3d(-width/2, height/2, lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(width/2, height/2, lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(width/2, -height/2, lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(-width/2, -height/2, lenght/2);
         glEnd();

		  // Right
		 glBegin(GL_QUADS);
		  glNormal3d(-width/2, 0, 0);
		  glTexCoord2f(0, 0);
		  glVertex3d(-width/2, +height/2, -lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(-width/2, +height/2, lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(-width/2, -height/2, lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

		  // Back
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, -lenght/2);
		  glTexCoord2f(0, 0);
		  glVertex3d(width/2, height/2, -lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(-width/2, height/2, -lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(-width/2, -height/2, -lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(width/2, -height/2, -lenght/2);
         glEnd();

		  // Left
		 glBegin(GL_QUADS);
		  glNormal3d(width/2, 0, 0);
		  glTexCoord2f(0, 0);
		  glVertex3d(width/2, height/2, lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(width/2, height/2, -lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(width/2, -height/2, -lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(width/2, -height/2, lenght/2);
         glEnd();

		 // Top
		 glBegin(GL_QUADS);
		  glNormal3d(0, height/2, 0);
		  glTexCoord2f(0, 0);
		  glVertex3d(-width/2, height/2, -lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(width/2, height/2, -lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(width/2, height/2, lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(-width/2, height/2, lenght/2);
         glEnd();

		 
		 // Bottom
		 glBegin(GL_QUADS);
		  glNormal3d(0, -height/2, 0);
		  glTexCoord2f(0, 0);
		  glVertex3d(-width/2, -height/2, lenght/2);
		  glTexCoord2f(1, 0);
		  glVertex3d(width/2, -height/2, lenght/2);
		  glTexCoord2f(1, 1);
          glVertex3d(width/2, -height/2, -lenght/2);
		  glTexCoord2f(0, 1);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

		 glDisable(GL_TEXTURE_2D);
}
void Geometry::box(GLdouble width, GLdouble height, GLdouble lenght,GLint* colors){
	 
		 // Front
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, lenght/2);
		  glColor3ub(colors[0],colors[1],colors[2]);
		  glVertex3d(-width/2, height/2, lenght/2);
		  glVertex3d(width/2, height/2, lenght/2);
          glVertex3d(width/2, -height/2, lenght/2);
          glVertex3d(-width/2, -height/2, lenght/2);
         glEnd();

		  // Right
		 glBegin(GL_QUADS);
		  glNormal3d(-width/2, 0, 0);
		  glColor3ub(colors[3],colors[4],colors[5]);
		  glVertex3d(-width/2, height/2, -lenght/2);
		  glVertex3d(-width/2, height/2, lenght/2);
          glVertex3d(-width/2, -height/2, lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

		  // Back
		 glBegin(GL_QUADS);
		  glNormal3d(0, 0, -lenght/2);
		  glColor3ub(colors[6],colors[7],colors[8]);
		  glVertex3d(width/2, height/2, -lenght/2);
		  glVertex3d(-width/2, height/2, -lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
         glEnd();

		  // Left
		 glBegin(GL_QUADS);
		  glNormal3d(width/2, 0, 0);
		  glColor3ub(colors[9],colors[10],colors[11]);
		  glVertex3d(width/2, height/2, lenght/2);
		  glVertex3d(width/2, height/2, -lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
          glVertex3d(width/2, -height/2, lenght/2);
         glEnd();

		 // Top
		 glBegin(GL_QUADS);
		  glNormal3d(0, height/2, 0);
		  glColor3ub(colors[12],colors[13],colors[14]);
		  glVertex3d(-width/2, height/2, -lenght/2);
		  glVertex3d(+width/2, height/2, -lenght/2);
          glVertex3d(+width/2, height/2, lenght/2);
          glVertex3d(-width/2, height/2, lenght/2);
         glEnd();

		 
		 // Bottom
		 glBegin(GL_QUADS);
		  glNormal3d(0, -height/2, 0);
		  glColor3ub(colors[15],colors[16],colors[17]);
		  glVertex3d(-width/2, -height/2, lenght/2);
		  glVertex3d(width/2, -height/2, lenght/2);
          glVertex3d(width/2, -height/2, -lenght/2);
          glVertex3d(-width/2, -height/2, -lenght/2);
         glEnd();

		 
	}

void Geometry::sphere(GLdouble radius,GLint slices, GLint stacks){

	// the sphere is made by quads, each quad is composed by two triangles
	glBegin(GL_TRIANGLES);
	for (GLfloat lat = 0; lat < stacks; lat += 1.f)
		for (GLfloat lon = 0; lon < slices; lon += 1.f)
		{
			Vector3 p;

			// -- first triangle
			p = spherePoint(lat, lon, stacks, slices,radius);
 	        glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);


			// -- second triangle
			p = spherePoint(lat, lon, stacks, slices,radius);
 	        glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

	}
	glEnd();
}

void Geometry::sphere(GLdouble radius,GLint slices, GLint stacks,GLint* colors){
    int color=1;
	// the sphere is made by quads, each quad is composed by two triangles
	glBegin(GL_TRIANGLES);
	for (GLfloat lat = 0; lat < stacks; lat += 1.f){
		if( color == 1){
				glColor3ub(colors[0],colors[1],colors[2]);
				color=2;
		}else{
				glColor3ub(colors[3],colors[4],colors[5]);
				color=1;
		}
		for (GLfloat lon = 0; lon < slices; lon += 1.f){
			Vector3 p;
			if( color == 1){
				glColor3ub(colors[0],colors[1],colors[2]);
				color=2;
			}else{
				glColor3ub(colors[3],colors[4],colors[5]);
				color=1;
			}
			// -- first triangle
			p = spherePoint(lat, lon, stacks, slices,radius);
 	        glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);


			// -- second triangle
			
			p = spherePoint(lat, lon, stacks, slices,radius);
 	        glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat + 1, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

			p = spherePoint(lat, lon + 1, stacks, slices,radius);
			glNormal3d(p.x,p.y,p.z);
			glVertex3d(p.x,p.y,p.z);

		}
	}
	glEnd();
}



void Geometry::drawLamp(GLfloat arm1Angle,GLfloat arm2Angle,GLfloat arm3Angle){
 
 Geometry geo;

 int colors1[]={0,127,255, 102,255,0};
 int colors2[]={    255,153,51, 255,153,51, 255,153,51, 
        255,153,51, 255,153,51, 255,153,51};
 
 glPushMatrix();

 float   HEIGHT_CYL_A  = 0.5f;//BASE CYLINDER HEIGHT
 float   HEIGHT_CYL_B	 = 1.5f;//MIDDLE CYLINDERS HEIGHT
 float   HEIGHT_CYL_C  = 0.5f;//TOP CYLINDER HEIGHT
 float   SPHERE_RADIUS = 0.5f;//SPHERE RADIUS
 
 /* Draw Base Start */
 glRotatef(arm1Angle,0,1,0);
 glTranslatef(0,HEIGHT_CYL_A/2,0);
 geo.box(2,HEIGHT_CYL_A,2,colors2);
 /* Draw Base End */

 /* Draw Sphere Joint Start*/
  glTranslatef(0,SPHERE_RADIUS/2+HEIGHT_CYL_A,0);
  geo.sphere(SPHERE_RADIUS,10,10,colors1);
 /* Draw Sphere Joint End*/

 /* Draw first middle Arm Start */
 glRotatef(-20,0,0,1);
 glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_B/2,0);
 geo.box(1,HEIGHT_CYL_B,1,colors2);
 /* Draw first middle Arm End */

 /* Draw Sphere Joint Start*/
  glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_B/2,0);
  geo.sphere(SPHERE_RADIUS,10,10,colors1);
 /* Draw Sphere Joint End*/

 /* Draw second middle Arm Start */
 glRotatef(arm2Angle,0,0,1);
 glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_B/2,0);
 geo.box(1,HEIGHT_CYL_B,1,colors2);
  /* Draw second middle Arm End */

  /* Draw Sphere Joint Start*/
  glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_B/2,0);
  geo.sphere(SPHERE_RADIUS,10,10,colors1);
 /* Draw Sphere Joint End*/

   /* Draw third Arm Start */
 glRotatef(arm3Angle,0,0,1);
 glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_C/2,0);


 geo.box(2,HEIGHT_CYL_C,2,colors2);
  /* Draw third  Arm End */

 /*Sphere emission*/
 GLfloat mat_emission[] = {0.5f, 0.8f, 0.9f, 0.0f};
 glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);
 
 /* Draw Sphere Final  Start*/
  glTranslatef(0,SPHERE_RADIUS+HEIGHT_CYL_C/2,0);
  geo.sphere(SPHERE_RADIUS,10,10,colors1);
  glEnable(GL_LIGHT1);
 /* Draw Sphere Final End*/
  
  /*Put light near the final sphere*/
  glEnable(GL_LIGHT1);
  GLfloat pos[]={ 0.0, 0.0, 0.0, 1.0};
  GLfloat color[]={ 1.0, 1.0, 1.0, 1.0};
  //glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0);// set cutoff angle
  glLightfv(GL_LIGHT1, GL_POSITION, pos); 
  glLightfv(GL_LIGHT1, GL_DIFFUSE, color);
  //glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 2); 


  GLfloat mat_emission2[] = {0, 0, 0, 0.0};
  glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission2);



 glPopMatrix();

}

Vector3 Geometry::spherePoint(GLdouble lat, GLdouble lon, GLdouble n_lat, GLdouble n_lon,
					GLdouble radius){
 return Vector3(
		radius*sin(PI * (lat/n_lat)) * cos(2 * PI * (lon/n_lon)), 
		radius*sin(PI * (lat/n_lat)) * sin(2 * PI * (lon/n_lon)), 
		radius*cos(PI * (lat/n_lat)));
}



/*Returns x,y,z coordinates as a Vector (angles in radiants)*/
Vector3 Geometry::getPoint(float out_angle,float int_angle, float out_radius,float int_radius){

 return	Vector3( ( out_radius+int_radius*cos(int_angle) )*cos(out_angle),
                 ( out_radius+int_radius*cos(int_angle) )*sin(out_angle),
		                                       int_radius*sin(int_angle));
  
}
/*Returns the normal vector (angles in radiants)*/
Vector3 Geometry::getPointNormal(float out_angle,float int_angle, float out_radius,float int_radius){
	
 Vector3 tangentOut( -sin(out_angle), 
	                  cos(out_angle), 
					              0);

 Vector3 tangentInt( cos(out_angle)*(-sin(int_angle)) ,
	                 sin(out_angle)*(-sin(int_angle)) ,
					 cos(int_angle) );
 
 Vector3 normal = tangentOut^tangentInt;

 return normal.Normalize();

}

/*Texture Mapping tx: 1-2*/
void Geometry::glTorusTexCoord(float out_angle,float int_angle,int tx){

 glTexCoord2f( out_angle/PI , (int_angle)/PI );
	
}