//#include <GL/freeglut.h>
//#include <GL/gl.h>
//
//bool ortho = true;
//bool teapot = true;
//bool wireframe = true;
//bool flat = true;
//
//GLdouble size = 5.0;
//
//GLfloat controlPoints[4][4][3];
//GLfloat knots[8] = { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0 };
//GLfloat edgePoints[5][2] = { {0.0, 0.0}, {1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0} }; /* there counter clockwise */
//GLfloat edgePointsPentagon[6][2] = { {0.8, 0.5}, {0.8, 0.1}, {0.1, 0.1}, {0.1, 0.5}, {0.5, 0.8}, {0.8, 0.5} }; /* and there clockwise */
//
//GLUnurbsObj* nurbRenderer;
//
//void init_surface(void)
//{
//	int u, v;
//	for (u = 0; u < 4; u++) {
//		for (v = 0; v < 4; v++) {
//			controlPoints[u][v][0] = 2.0 * ((GLfloat)u - 1.5);
//			controlPoints[u][v][1] = 2.0 * ((GLfloat)v - 1.5);
//
//			if ((u == 1 || u == 2) && (v == 1 || v == 2))
//				controlPoints[u][v][2] = 3.0;
//			else
//				controlPoints[u][v][2] = -3.0;
//		}
//	}
//}
//
//void init()
//{
//	GLfloat mat_diffuse[] = { 0.7, 0.7, 0.7, 1.0 };
//	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
//	GLfloat mat_shininess[] = { 100.0 };
//
//	glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
//	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_AUTO_NORMAL);
//	glEnable(GL_NORMALIZE);
//	glEnable(GL_COLOR_MATERIAL);
//
//	glClearColor(0.0, 0.0, 0.0, 0.0);
//	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
//	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
//	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
//
//	init_surface();
//
//	nurbRenderer = gluNewNurbsRenderer();
//	gluNurbsProperty(nurbRenderer, GLU_SAMPLING_TOLERANCE, 25.0);
//	gluNurbsProperty(nurbRenderer, GLU_DISPLAY_MODE, GLU_FILL);
//}
//
//void displayTeapot()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	if (ortho) {
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		glOrtho(-15.0f, 15.0f, -15.0f, 15.0f, -15.0f, 15.0f);
//	}
//	else
//	{
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		gluPerspective(60.0f, 1.0f, 0.1f, 100.0f);
//		gluLookAt(0.0f, 0.0f, 50.0f,
//			0.0f, 0.0f, 0.0f,
//			0.0f, 1.0f, 0.0f);
//	}
//
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//
//	glPushMatrix();
//	glTranslatef(0.0, 0.0, -4.5);
//	glScalef(1.0, 1.0, 1.0);
//	glColor3f(0.5f, 0.0f, 0.8f);
//
//	if (wireframe)
//		glutSolidTeapot(size);
//	else
//		glutWireTeapot(size);
//
//	glPopMatrix();
//}
//
//void displaySurface()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	if (ortho) {
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		glOrtho(-15.0f, 15.0f, -15.0f, 15.0f, -15.0f, 15.0f);
//	}
//	else
//	{
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		gluPerspective(60.0f, 1.0f, 0.1f, 100.0f);
//		gluLookAt(0.0f, 0.0f, 50.0f,
//			0.0f, 0.0f, 0.0f,
//			0.0f, 1.0f, 0.0f);
//	}
//
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//
//	if (wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
//
//	glPushMatrix();
//	glColor3f(0.8f, 0.0f, 0.5f);
//
//	gluBeginSurface(nurbRenderer);
//	gluNurbsSurface(nurbRenderer, 8, knots, 8, knots, 4 * 3, 3, &controlPoints[0][0][0], 4, 4, GL_MAP2_VERTEX_3);
//
//	//Boundaries
//	gluBeginTrim(nurbRenderer);
//	gluPwlCurve(nurbRenderer, 5, &edgePoints[0][0], 2, GLU_MAP1_TRIM_2);
//	gluEndTrim(nurbRenderer);
//
//	//Actual pentagon
//	gluBeginTrim(nurbRenderer);
//	gluPwlCurve(nurbRenderer, 6, &edgePointsPentagon[0][0], 2, GLU_MAP1_TRIM_2);
//	gluEndTrim(nurbRenderer);
//
//	gluEndSurface(nurbRenderer);
//	glPopMatrix();
//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//}
//
//void display()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glShadeModel(flat ? GL_FLAT : GL_SMOOTH);
//
//	if (teapot)
//		displayTeapot();
//	else
//		displaySurface();
//
//	glFlush();
//}
//
//void reshape(int w, int h)
//{
//	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(45.0, (GLdouble)w / (GLdouble)h, 3.0, 8.0);
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//	glTranslatef(0.0, 0.0, -5.0);
//}
//
//void keyboard(unsigned char key, int x, int y)
//{
//	switch (key) {
//	case 'o':
//	case 'O':
//		ortho = !ortho;
//		glutPostRedisplay();
//		break;
//	case 't':
//	case 'T':
//		teapot = !teapot;
//		glutPostRedisplay();
//		break;
//	case 'w':
//	case 'W':
//		wireframe = !wireframe;
//		glutPostRedisplay();
//		break;
//	case 'f':
//	case 'F':
//		flat = !flat;
//		glutPostRedisplay();
//		break;
//	case 27:
//		exit(0);
//		break;
//	default:
//		break;
//	}
//}
//
//int main(int argc, char** argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
//	glutInitWindowSize(800, 800);
//	glutInitWindowPosition(100, 100);
//	glutCreateWindow(argv[0]);
//	init();
//	glutReshapeFunc(reshape);
//	glutDisplayFunc(display);
//	glutKeyboardFunc(keyboard);
//	glutMainLoop();
//	return 0;
//}