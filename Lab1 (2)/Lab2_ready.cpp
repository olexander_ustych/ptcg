//#include <GL/freeglut.h>
//#include <iostream>
//#include <sstream>
//#include <string>
//#include <math.h>
//
//using namespace std;
// 
//float PI = 3.14159f;
//float FieldWidth = 12;
//float FieldHeight = 12;
//float HalfFieldWidth = FieldWidth / 2;
//float HalfFieldHeight = FieldHeight / 2;
//float OnePercentOfHalfWidth = (float)HalfFieldWidth / 100;
//float OnePercentOfHalfHeight = (float)HalfFieldHeight / 100;
//
//float currentAngle = 0.0f;
//float aCoeff = 1.0f;
// 
//void drawText(float x, float y, string text, float r, float g, float b)
//{
//    glColor3f(r, g, b);
//    glRasterPos2f(x, y);
//    glutBitmapString(GLUT_BITMAP_8_BY_13, (const unsigned char*)text.c_str());
//}
// 
//void drawLine(
//    float x0, float y0, float x1, float y1,
//    float r, float g, float b)
//{
//    glColor3f(r, g, b);
//    glBegin(GL_LINES);
//    {
//        glVertex2f(x0, y0);
//        glVertex2f(x1, y1);
//    }
//    glEnd();
//}
// 
//void drawSin(float r, float g, float b)
//{
//    const float step = (float)PI / 20;
// 
//    float xPrev = -1 * (float)PI;
// 
//    for (size_t i = 0; i < 40; i++)
//    {
//        float x0 = xPrev;
//        float y0 = aCoeff * sin(x0);
// 
//        float x1 = x0 + step;
//        float y1 = aCoeff * sin(x1);
//        xPrev = x1;
// 
//        drawLine(x0, y0, x1, y1, r, g, b);
//    }
//}
//
//void drawAxisArrow(float x, float y, float offsetX, float offsetY, float angle) {
//    glPushMatrix();
//    glTranslatef(offsetX, offsetY, 0);
//    glRotatef(-angle, 0, 0, 1);
//    drawLine(x, y, 0.0f, 0.0f, 1, 1, 1);
//    glPopMatrix();
//
//    glPushMatrix();
//    glTranslatef(offsetX, offsetY, 0);
//    glRotatef(angle, 0, 0, 1);
//    drawLine(x, y, 0.0f, 0.0f, 1, 1, 1);
//    glPopMatrix();
//}
// 
//void drawCoordinates(float r, float g, float b)
//{
//    float halfOfLengthOfLine = HalfFieldHeight / 2;
//
//    //axes
//    drawLine(-HalfFieldWidth, 0, HalfFieldWidth, 0, 1, 1, 1);
//    drawText(HalfFieldWidth - 10 * OnePercentOfHalfWidth, -10 * OnePercentOfHalfHeight, "X", 1, 1, 1);
//
//    drawAxisArrow(-0.5f, 0.0f, HalfFieldWidth, 0.0f, 35.0f);
//    drawAxisArrow(0.0f, -0.5f, 0.0f, HalfFieldHeight, 35.0f);
//
//    drawLine(0, -HalfFieldHeight, 0, HalfFieldHeight, 1, 1, 1);
//    drawText(-10 * OnePercentOfHalfWidth, HalfFieldHeight - 15 * OnePercentOfHalfHeight, "Y", 1, 1, 1);
// 
//    //indicators
//    drawText(1.5f * OnePercentOfHalfWidth, 1.5f * OnePercentOfHalfHeight, "0" , r, g, b);
//
//    //x
//    for (float i = -HalfFieldWidth + 1.0f; i < HalfFieldWidth; i++)
//    {
//        if (i == 0) {
//            continue;
//        }
//
//        drawLine(i, -2.0f * OnePercentOfHalfHeight, i, 2.0f * OnePercentOfHalfHeight, 1, 1, 1);
//
//        ostringstream oss; 
//        oss << (int)i;
//        auto values = _strdup(oss.str().c_str());
//        const int valuesWidthText = glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)values);
//        drawText(i, 3.5f * OnePercentOfHalfHeight, oss.str(), r, g, b);
//    }
//
//    //y
//    for (float i = -HalfFieldHeight + 1.0f; i < HalfFieldHeight; i++)
//    {
//        if (i == 0) {
//            continue;
//        }
//
//        drawLine(-2.0f * OnePercentOfHalfWidth, i, 2.0f * OnePercentOfHalfWidth, i, 1, 1, 1);
//
//        ostringstream oss;
//        oss << (int)i;
//        auto values = _strdup(oss.str().c_str());
//        //const int valuesWidthText = glutBitmapLength(GLUT_BITMAP_8_BY_13, (const unsigned char*)values);
//        drawText(3.5f * OnePercentOfHalfWidth, i, oss.str(), r, g, b);
//    } 
// 
//    //parameters of angle and [a]
//    drawText(-36 * OnePercentOfHalfWidth, -HalfFieldHeight + 0.1, "a: " + to_string(aCoeff), r, g, b);
//    drawText(36 * OnePercentOfHalfWidth, -HalfFieldHeight + 0.1, "Angle: " + to_string(currentAngle), r, g, b);
//}
// 
//void draw()
//{
//    glClear(GL_COLOR_BUFFER_BIT);
// 
//    drawCoordinates(1, 1, 1);
//
//    //draw sinus and rotate it
//    glPushMatrix();
//        glRotatef(currentAngle, 0.0f, 0.0f, 1.0f);
//        drawSin(0.5, 0.5, 1);
//    glPopMatrix();
//
//    glutSwapBuffers();
//}
// 
//
//void keyboard(unsigned char key, int x, int y) {
//    switch (key) {
//    case 'd': //reverse clockwise
//        currentAngle -= 45;
//        glutPostRedisplay();
//        break;
//    case 'a': //clockwise
//        currentAngle += 45;
//        glutPostRedisplay();
//        break;
//    }
//}
// 
//int main(int argc, char** argv)
//{
//    glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
//    glutInitWindowSize(800, 800);
//    glutCreateWindow("asin(x)");
//
//    cout << "Ender a value: ";
//    cin >> aCoeff;
//
//    double halfWidth = FieldWidth / 2;
//    double halfHeight = FieldHeight / 2;
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    glOrtho(-halfWidth, halfWidth, -halfHeight, halfHeight, 100, -100);
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    glutDisplayFunc(draw);
//    glutKeyboardFunc(keyboard);
//
//    glutMainLoop();
//    return 0;
//}