//#include <GL/glut.h>
//
//GLfloat light_position[] = { 5.0, 9.5, 4.0, 1.0 }; //позиція світла
//GLfloat light_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
//GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
//GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
//GLfloat light_attenuation[] = { 0.4, 0.6, 1.8 }; //радіальне згасання
//
//void display()
//{
//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//
//    //перспективна проєкція
//    gluPerspective(45.0, 1.0, 0.1, 100.0);
//
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//    gluLookAt(0.0, 0.0, -15.0,
//        0.0, 0.0, 0.0,
//        0.0, 1.0, 0.0);
//
//    //лінійний туман
//    glFogf(GL_FOG_START, 1.0f);
//	glFogf(GL_FOG_END, 4.0f);
//	glFogi(GL_FOG_MODE, GL_LINEAR);
//
//    //каркасний куб
//    glPushMatrix();
//    glRotatef(30, 1.0, 1.0, 1.0);
//    glTranslatef(-2, -2, 0);
//    glutWireCube(1.0);
//    glPopMatrix();
//
//    //звичайний куб
//    glPushMatrix();
//    glTranslatef(2, 2, 0);
//    glRotatef(30, 1.0, 1.0, 1.0);
//    glutSolidCube(1.0);
//    glPopMatrix();
//
//    glFlush();
//}
//
//void init()
//{
//    glClearColor(0, 0, 0, 0.0);
//
//    //ініціалізація параметрів світла
//    //позиція, ембієнт, дифьюз і т.п
//    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
//    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
//    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
//
//    //прописуємо радіальне згасання
//    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, light_attenuation[0]);
//    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, light_attenuation[1]);
//    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, light_attenuation[2]);
//}
//
//int main(int argc, char** argv)
//{
//    glutInit(&argc, argv);
//    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
//    glutInitWindowSize(500, 500);
//    glutCreateWindow("Task 20");
//
//    init();
//
//    glEnable(GL_DEPTH_TEST);
//    glutDisplayFunc(display);
//    glutMainLoop();
//    return 0;
//}