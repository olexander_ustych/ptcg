//#include <GL/freeglut.h>
//#include <GL/gl.h>
//#include <iostream>
//
//#include "stb_image.h"
//#include "vector3.h"
//#include "Geometry.h"
//using namespace std;
//
//
//#define PI 3.14159265359
//const char* filename = "D://tree-texture.jpg";
//
//bool fogType = false;
//bool drawRadialFading = false;
//
//GLuint textureID;
//GLdouble size = 5.0;
//GLfloat angle = 0;
//GLfloat rotateSpeed = 1.0f;
//
//GLfloat light_position[] = { .0, .0, -15.0, 0.0 };
//GLfloat light_ambient[] = { 0.0, 0.0, 1.0, 1.0 };
//GLfloat light_diffuse[] = { 0.0, 0.0, 1.0, 1.0 };
//
//GLfloat spotlight_position[] = { 0.0f, 0.0f, 10.0f, 1.0f };
//GLfloat spotlight_direction[] = { 0.0f, 0.0f, -1.0f, 1.0f };
//GLfloat spotlight_ambient[] = { 0.0f, 1.0f, 0.0f, 1.0f };
//GLfloat spotlight_diffuse[] = { 0.0f, 1.0f, 0.0f, 1.0f };
//GLfloat spotlight_cutoff = 2.0f;
//GLfloat spotlight_exponent = 10.0f;
//
//GLfloat distant_light_direction[] = { 1.0f, 0.0f, -1.0f, 1.0f };
//GLfloat distant_light_ambient[] = { 1.0f, 0.0f, 0.0f, 1.0f };
//GLfloat distant_light_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
//
//GLfloat materialAmbient[] = { 1.0, 1.0, 1.0, 0.0 };
//GLfloat materialDiffuse[] = { 0.8, 0.8, 0.8, 1.0 };
//GLfloat materialSpecular[] = { 1.0, 1.0, 1.0, 1.0 };
//GLfloat shininess[] = { 80.0f };
//
//void loadTexture(const char* filename)
//{
//	int width, height, nrChannels;
//	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
//
//	glGenTextures(0, &textureID);
//	//glBindTexture(GL_TEXTURE_2D, textureID);
//
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//	if (data)
//	{
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
//	}
//	else
//	{
//		std::cout << "Failed to load texture" << std::endl;
//	}
//
//	stbi_image_free(data);
//}
//
//void init()
//{
//	glClearColor(0.2, 0.2, 0.4, 0.0);
//	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
//	glEnable(GL_LIGHT1);
//	glEnable(GL_LIGHT2);
//	
//
//	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
//	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
//	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
//
//	//spotlight
//	glLightfv(GL_LIGHT1, GL_POSITION, spotlight_position);
//	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotlight_direction);
//	glLightfv(GL_LIGHT1, GL_AMBIENT, spotlight_ambient);
//	glLightfv(GL_LIGHT1, GL_DIFFUSE, spotlight_diffuse);
//	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, spotlight_cutoff);
//	glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, spotlight_exponent);
//
//	//distant
//	glLightfv(GL_LIGHT2, GL_POSITION, distant_light_direction);
//	glLightfv(GL_LIGHT2, GL_AMBIENT, distant_light_ambient);
//	glLightfv(GL_LIGHT2, GL_DIFFUSE, distant_light_diffuse);
//
//	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
//	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
//	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
//	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
//
//	loadTexture(filename);
//}
//
//void drawSimpleTorus() {
//	glPushMatrix();
//	glColor3f(0, 0.8f, -5);
//
//	glRotatef(rotateSpeed, 1.0, 0.0, 0.0);
//	glTranslatef(0, 5, 0);
//	glutSolidTorus(1.5f, 5.0f, 50, 50);
//	
//	glPopMatrix();
//}
//
//void drawTexturedTorus() 
//{
//	glPushMatrix();
//	glColor3f(1.0f, 1.0f, 1.0f);
//
//	glRotatef(rotateSpeed, 1, 0, 0);
//	glTranslatef(0, -5, 0);
//
//	Geometry geometry;
//	geometry.torus(5.0f, 1.5f, textureID);
//
//	glPopMatrix();
//}
//
//void drawFog(float r, float g, float b)
//{
//	glEnable(GL_FOG);
//	if (fogType)
//	{
//		glFogf(GL_FOG_START, 1.0f);
//		glFogf(GL_FOG_END, 4.0f);
//		glFogi(GL_FOG_MODE, GL_LINEAR);
//	}
//	else
//	{
//		glFogf(GL_FOG_DENSITY, 0.5f);
//		glFogi(GL_FOG_MODE, GL_EXP);
//	}
//	GLfloat fogColor[4] = { r, g, b, 1.0f };
//	glFogfv(GL_FOG_COLOR, fogColor);
//	
//	glClearColor(r, g, b, 1.0f);
//}
//
//void radialFading() {
//	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.0);
//	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05);
//	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.001);
//}
//
//
//void displayToruses()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	//camera setup
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//
//	gluPerspective(60.0f, 1.0f, 0.1f, 100.0f);
//	gluLookAt(0.0f, 0.0f, 25.0f,
//		0, 0, 0,
//		0, 1, 0);
//
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//
//	if (drawRadialFading) {
//		radialFading();
//	}
//
//	drawFog(144.0f / 255.0f, 238.0f / 255.0f, 144.0f / 255.0f);
//
//	//first storus
//	drawSimpleTorus();
//	//second torus
//	drawTexturedTorus();
//}
//
//
//void display()
//{
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glShadeModel(GL_SMOOTH);
//
//	displayToruses();
//
//	glFlush();
//
//	if (rotateSpeed > 360.0f) {
//		rotateSpeed -= 360.0f;
//	}
//
//	rotateSpeed += 1.0f;
//}
//
//
//void reshape(int w, int h)
//{
//	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(45.0, (GLdouble)w / (GLdouble)h, 3.0, 8.0);
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//	glTranslatef(0.0, 0.0, -5.0);
//}
//
//void keyboard(unsigned char key, int x, int y)
//{
//	switch (key) {
//	case 'f':
//		fogType = !fogType;
//		glutPostRedisplay();
//		break;
//	case 'y':
//
//		glutPostRedisplay();
//		break;
//	case 'r':
//		drawRadialFading = !drawRadialFading;
//		glutPostRedisplay();
//		break;
//	case 'a':
//		if (glIsEnabled(GL_LIGHT0))
//		{
//			glDisable(GL_LIGHT0);
//		}
//		else
//		{
//			glEnable(GL_LIGHT0);
//		}
//		break;
//	case 's':
//		if (glIsEnabled(GL_LIGHT1))
//		{
//			glDisable(GL_LIGHT1);
//		}
//		else
//		{
//			glEnable(GL_LIGHT1);
//		}
//		break;
//	case 'd':
//		if (glIsEnabled(GL_LIGHT2))
//		{
//			glDisable(GL_LIGHT2);
//		}
//		else
//		{
//			glEnable(GL_LIGHT2);
//		}
//		break;
//	case 27:
//		exit(0);
//		break;
//	default:
//		break;
//	}
//}
//
//void timer(int a) {
//	glutPostRedisplay();
//	glutTimerFunc(10, timer, 0);
//}
//
//int main(int argc, char** argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
//	glutInitWindowSize(800, 800);
//	glutInitWindowPosition(100, 100);
//	glutCreateWindow(argv[0]);
//	init();
//	glutReshapeFunc(reshape);
//	glutDisplayFunc(display);
//	glutTimerFunc(10, timer, 0);
//	glutKeyboardFunc(keyboard);
//	glutMainLoop();
//	return 0;
//}