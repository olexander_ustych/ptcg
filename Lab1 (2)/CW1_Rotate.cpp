//#include <iostream>
//#include <GL/freeglut.h>
//#include <cstdlib>
//using namespace std;
//
//#pragma region ZPKG_KR
//
//struct Point {
//	double x, y, z;
//};
//
//struct Vector {
//	double x, y, z;
//};
//
//struct Color {
//	double r, g, b;
//};
//
//Point MoveForVector(Point point, Vector vector) {
//	Point resultPoint;
//	resultPoint.x = point.x + vector.x;
//	resultPoint.y = point.y + vector.y;
//	resultPoint.z = point.z + vector.z;
//	return resultPoint;
//}
//
//
//void uselessDisplay() {
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // очистити буфери
//	glMatrixMode(GL_MODELVIEW); // перейти до режиму моделі
//	glLoadIdentity(); // скинути матрицю
//	gluLookAt(0.0f, 0.0f, 5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f); // встановити камеру
//	glColor3f(1, 0, 0);
//	glBegin(GL_POINTS); // почати малювання точки
//	Point APointStart, APointEnd;
//	APointStart.x = 1;
//	APointStart.y = 2;
//	APointStart.z = 3;
//	Vector moveVector;
//	moveVector.x = 5;
//	moveVector.y = 3;
//	moveVector.z = 7;
//	APointEnd = MoveForVector(APointStart, moveVector);
//	glVertex3f(APointStart.x, APointStart.y, APointStart.z); // тривимірна точка
//	glVertex3f(APointEnd.x, APointEnd.y, APointEnd.z); // тривимірна точка
//	glEnd(); // закінчити малювання
//	glPointSize(5);
//	glutSwapBuffers(); // показати екран
//}
//
//void drawGradienLine(Point pointStart, Point pointEnd, Color colorStart, Color colorEnd) {
//	glShadeModel(GL_SMOOTH);
//	glBegin(GL_LINES);
//	glColor3f(colorStart.r, colorStart.g, colorStart.b); // start color
//	glVertex3f(pointStart.x, pointStart.y, pointStart.z); // початкова точка
//	glColor3f(colorEnd.r, colorEnd.g, colorEnd.b); // end color
//	glVertex3f(pointEnd.x, pointEnd.y, pointEnd.z); // кінцева точка
//	glEnd();
//}
//
//void drawStripeLine(Point pointStart, Point pointEnd, int Stripes, Color colorLine) {
//	glLineStipple(Stripes, 0x00FF);
//	glEnable(GL_LINE_STIPPLE);
//	glBegin(GL_LINES);
//	glColor3f(colorLine.r, colorLine.g, colorLine.b); // color line
//	glVertex3f(pointStart.x, pointStart.y, pointStart.z); // початкова точка
//	glVertex3f(pointEnd.x, pointEnd.y, pointEnd.z); // кінцева точка
//	glEnd();
//
//	glDisable(GL_LINE_STIPPLE);
//}
//
//float angle = 0.0f;
//float lx = 0.0f, ly = 0.0f, lz = -1.0f;
//float x = 0.0f, y = 1.0f, z = 5.0f;
//int window_width = 800;
//int window_height = 600;
//
//void display2() {
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//	gluLookAt(x, y, z, x + lx, y + ly, z + lz, 0.0f, 1.0f, 0.0f);
//
//	glPushMatrix();
//
//	Point APointStart, APointEnd;
//	APointStart.x = 1;
//	APointStart.y = 2;
//	APointStart.z = 0;
//
//	APointEnd.x = 1;
//	APointEnd.y = 0;
//	APointEnd.z = 0;
//
//	Color colorStart, colorEnd;
//	colorStart.r = 0;
//	colorStart.g = 1;
//	colorStart.b = 0;
//
//	colorEnd.r = 1;
//	colorEnd.g = 0;
//	colorEnd.b = 0;
//
//	drawGradienLine(APointStart, APointEnd, colorStart, colorEnd);
//	//drawStripeLine(APointStart, APointEnd, 1, colorEnd);
//
//	// визначення проекції лінії на екран
//	double modelview[16];
//	double projection[16];
//	int viewport[4];
//	GLdouble winX, winY, winZ;
//	GLdouble posX, posY, posZ;
//	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
//	glGetDoublev(GL_PROJECTION_MATRIX, projection);
//	glGetIntegerv(GL_VIEWPORT, viewport);
//	gluProject(0.0, 0.0, 0.0, modelview, projection, viewport, &posX, &posY, &posZ);
//	gluProject(1.0, 0.0, 0.0, modelview, projection, viewport, &winX, &winY, &winZ);
//	double screenX = ((winX - viewport[0]) / (double)viewport[2]) * window_width;
//	double screenY = ((viewport[3] - winY) / (double)viewport[3]) * window_height;
//	double screenZ = winZ;
//	glPushMatrix();
//	glLoadIdentity();
//	glBegin(GL_LINES);
//	glVertex3d(posX, posY, posZ);
//	glVertex3d(screenX, screenY, screenZ);
//	glEnd();
//	glPopMatrix();
//
//	glutSwapBuffers();
//}
//void reshape(int width, int height) {
//	if (height == 0) {
//		height = 1;
//	}
//	window_width = width;
//	window_height = height;
//	glViewport(0, 0, window_width, window_height);
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(45.0f, (GLfloat)window_width / (GLfloat)window_height, 0.1f, 100.0f);
//}
//#pragma endregion
//
//int main(int argc, char** argv)
//{
//	// window init 
//	int winPosX, winPosY, winSizeX, winSizeY;
//	winPosX = 300;
//	winPosY = 80;
//	winSizeX = 500;
//	winSizeY = 500;
//	//// string winName = "KR-ZPKG";
//
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
//	//glutInitWindowSize(window_width, window_height);
//	glutInitWindowPosition(winPosX, winPosY);
//	glutInitWindowSize(winSizeX, winSizeY);
//	glutCreateWindow("ZPKG KR");
//	glEnable(GL_DEPTH_TEST);
//	glutDisplayFunc(display2);
//	glutReshapeFunc(reshape);
//	glutMainLoop();
//	return 0;
//}
//
//
//#pragma region Trapezoid
///*TrapezoidRotation::staticSetMetrics();
//
//glutInit(&argc, argv);
//glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
//glutInitWindowPosition(300, 80);
//glutInitWindowSize(500, 500);
//glutCreateWindow("Trapezoid");
//
//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
//glMatrixMode(GL_PROJECTION);
//glLoadIdentity();
//gluOrtho2D(-5.0f, 5.0f, -5.0f, 5.0f);
//
//glutDisplayFunc(TrapezoidRotation::staticDisplay);
//glutTimerFunc(0, TrapezoidRotation::staticUpdate, 0);
//
//glutMainLoop();
//
//return 0;*/
//#pragma endregion
//
//#pragma region Triangle
////void reshape(int w, int h) {
////    glViewport(0, 0, w, h);
////    glMatrixMode(GL_PROJECTION);
////    glLoadIdentity();
////    gluOrtho2D(-1.0, 1.0, -1.0, 1.0);
////    glMatrixMode(GL_MODELVIEW);
////}
////
////int main(int argc, char** argv) {
////    glutInit(&argc, argv);
////    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
////
////    glutInitWindowPosition(300, 80);
////    glutInitWindowSize(500, 500);
////    glutCreateWindow("Triangles");
////
////    glutDisplayFunc(TriangleBuilder::staticDisplay);
////    glutMouseFunc(TriangleBuilder::staticMouse);
////
////    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION); 
////
////    glutMainLoop();
////}
//#pragma endregion