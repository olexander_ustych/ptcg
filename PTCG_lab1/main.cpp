#include<stdio.h> 
#include<gl/glut.h> 
#include<math.h> 
#include <string.h>
#include <stdlib.h>

// Global variables to store color information
GLfloat wallColor[] = { 0.8, 0.8, 0.6 }; // Default wall color
GLfloat roofColor[] = { 0.8, 0.2, 0.2 }; // Default roof color
GLfloat treeColor[] = { 0.2, 0.8, 0.2 }; // Default tree crown color
GLfloat treeBaseColor[] = { 0.6, 0.3, 0.1 }; // Default wall color
GLfloat* allColors[] = {
    wallColor, roofColor, treeBaseColor, treeColor
};

bool text = true;

// Function to draw a stylized house
void drawHouse() {
    // Draw walls
    glColor3fv(wallColor);
    glBegin(GL_QUADS);
    glVertex2f(-0.4, -0.4);
    glVertex2f(0.4, -0.4);
    glVertex2f(0.4, 0.4);
    glVertex2f(-0.4, 0.4);
    glEnd();

    // Draw roof
    glColor3fv(roofColor);
    glBegin(GL_TRIANGLES);
    glVertex2f(-0.5, 0.4);
    glVertex2f(0.5, 0.4);
    glVertex2f(0.0, 1.0);
    glEnd();

    if (text) 
    {
        // Draw label
        glColor3f(0.0, 0.0, 0.0); // Black color for text
        glRasterPos2f(0.0, 0.0); // Position for the label
        char houseLabel[] = "House";
        int len = strlen(houseLabel);
        for (int i = 0; i < len; i++) {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, houseLabel[i]);
        }
    }
}

// Function to draw a stylized tree
void drawTree() {
    // Draw trunk
    glColor3fv(treeBaseColor); // Brown color for trunk
    glRectf(-0.1, -0.25, 0.1, 0.25);

    // Draw crown
    glColor3fv(treeColor);
    glBegin(GL_TRIANGLES);
    glVertex2f(-0.35, 0.0);
    glVertex2f(0.35, 0.0);
    glVertex2f(0.0, 0.35);
    glEnd();

    if (text)
    {
        // Draw label
        glColor3f(0.0, 0.0, 0.0); // Black color for text
        glRasterPos2f(0.0, 0.0); // Position for the label
        char treeLabel[] = "Tree";
        int len = strlen(treeLabel);
        for (int i = 0; i < len; i++) {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, treeLabel[i]);
        }
    }
}

// Function to display the scene
void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw house
    drawHouse();

    // Draw tree    
    glPushMatrix();
    glTranslatef(0.7, 0.7, 0.0); // Translate to position the tree        
    drawTree();
    glPopMatrix();

    glPushMatrix();        
    glTranslatef(-0.7, 0.7, 0.0); // Translate to position the tree        
    drawTree();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.7, -0.7, 0.0); // Translate to position the tree
    drawTree();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.7, -0.7, 0.0); // Translate to position the tree
    drawTree();
    glPopMatrix();
    

    glFlush();
}

//Clear all objects
void clearAllObjects(int next)
{    
    size_t size = 0;
    while (allColors[size] != nullptr) {
        auto a = allColors[size];
        size++;
    }

    if (next >= size - 1) 
    {
        exit(0);
    }
    
    allColors[next][0] = 1.0;
    allColors[next][1] = 1.0;
    allColors[next][2] = 1.0;

    if (next == 0)
    {
        text = false;
    }

    glutTimerFunc(1000, clearAllObjects, ++next);
    glutPostRedisplay();
}

// Function to handle keyboard input
void keyboard(unsigned char key, int x, int y) {
    switch (key) {
    case 'w': // Change wall color to white
        for (GLfloat& var : wallColor)
        {
            var = 0.0;
        }
        break;
    case 'r': // Change roof color to blue
        roofColor[0] = 0.2;
        roofColor[1] = 0.2;
        roofColor[2] = 0.8;
        break;
    case 'g': // Change tree crown color to yellow
        treeColor[0] = 0.8;
        treeColor[1] = 0.8;
        treeColor[2] = 0.2;
        break;
    case 'c': // Clear colors to default       
        wallColor[0] = 0.8;
        wallColor[1] = 0.8;
        wallColor[2] = 0.6;
        roofColor[0] = 0.8;
        roofColor[1] = 0.2;
        roofColor[2] = 0.2;
        treeColor[0] = 0.2;
        treeColor[1] = 0.8;
        treeColor[2] = 0.2;
        break;
    case 27: // ESC key to exit
        clearAllObjects(0);
        glutPostRedisplay();

        //exit(0);        
        break;
    }
    glutPostRedisplay(); // Request a redraw
}

// Function to initialize OpenGL
void init() {
    glClearColor(1.0, 1.0, 1.0, 1.0); // Set background color to white
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1.0, 1.0, -1.0, 1.0); // Set up 2D orthographic projection
}

// Main function
int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1000, 1000); // Set window size
    glutCreateWindow("OpenGL Stylized House and Tree");

    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMainLoop();

    return 0;
}



























/*void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw square
    glBegin(GL_QUADS);
    glColor3f(0.0, 0.0, 0.0); // Set color
    glVertex2f(-0.5, -0.5);   // Bottom-left vertex
    glVertex2f(0.5, -0.5);    // Bottom-right vertex
    glVertex2f(0.5, 0.5);     // Top-right vertex
    glVertex2f(-0.5, 0.5);    // Top-left vertex
    glEnd();

    // Draw triangle
    glBegin(GL_TRIANGLES);
    glColor3f(1.0, 0.0, 0.0); // Set color
    glVertex2f(-0.5, 0.5);     // Bottom-left vertex
    glVertex2f(0.0, 1.0);     // Top vertex
    glVertex2f(0.5, 0.5);    // Bottom-right vertex
    glEnd();

    //


    glFlush();
}

void init() {
    glClearColor(1.0, 1.0, 1.0, 1.0); // Set background color to white
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1.0, 1.0, -1.0, 1.0); // Set up 2D orthographic projection
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1000, 1000); // Set window size
    glutCreateWindow("OpenGL Square and Triangle");

    init();
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}*/

