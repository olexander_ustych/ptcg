#include<gl/glut.h> 
#include <cstdio>
#include <SOIL.h>

float eyeX = 10.0f;
float eyeY = -10.0f;
float eyeZ = 0.0f;
float centerX = 0.0f;
float centerY = 0.0f;
float centerZ = 0.0f;
float upX = 0.0f;
float upY = 1.0f;
float upZ = 0.0f;

float angle = 45.0f, _angle = 0;
float radius = 5.0f;

bool move = true;

// Load image data into an OpenGL texture
GLuint LoadTexture(const char* filename) {
    // Code to load image data from file and create an OpenGL texture
    
    GLuint textureId = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_TEXTURE_REPEATS);
    if (textureId == 0) {
        printf("Error loading texture: %s\n", SOIL_last_result());
    }

    return textureId;
}

// Set the ambient lighting properties
GLfloat ambientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Low ambient light level

// Set the material properties
GLfloat materialColor[] = { 0.8f, 0.8f, 0.8f, 1.0f }; // Object color
GLfloat materialAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Object ambient reflection

void initAmbientLight() {
    glEnable(GL_LIGHTING);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Set the background color
    glShadeModel(GL_SMOOTH); // Enable smooth shading

    // Set the ambient light properties
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

    // Set the material properties
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materialColor);
    glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
}

void initPointLight() {    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // Point light
    GLfloat light_position[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}

void initSpotLight() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);

    // Spotlight
    GLfloat spotlight_position[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Spotlight position
    glLightfv(GL_LIGHT1, GL_POSITION, spotlight_position);
    GLfloat spotlight_diffuse[] = { 0.0f, 1.0f, 0.0f, 1.0f }; // Spotlight diffuse color
    glLightfv(GL_LIGHT1, GL_DIFFUSE, spotlight_diffuse);
    GLfloat spot_direction[] = { -1.0f, -1.0f, 0.0f }; // Spotlight direction
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 70.0f); // Spotlight cutoff angle
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 200.0f); // Spotlight exponent

    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}

void initDistantLight() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT2);

    GLfloat distant_light_direction[] = { 100.0f, 100.0f, 100.0f }; // Distant light direction
    glLightfv(GL_LIGHT2, GL_POSITION, distant_light_direction);
    GLfloat distant_light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Distant light diffuse color
    glLightfv(GL_LIGHT2, GL_DIFFUSE, distant_light_diffuse);
    glLightfv(GL_LIGHT2, GL_SPECULAR, distant_light_diffuse);//���� ���������

    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}

// Render a textured sphere
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 2, 2, 100);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);    


    /*
    glEnable(GL_FOG);
    GLfloat fogColor[] = { 0.5f, 0.5f, 1.0f, 1.0f }; // ���� ������ (����)
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_MODE, GL_EXP); // ����� ������: ��������������
    glFogf(GL_FOG_DENSITY, 0.1f); // ������� ������
    */


    // Load the texture
    GLuint textureId = LoadTexture("My_profile.png");

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);
    
    // Set up texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glPushMatrix();

    glRotatef(0.0f, angle, 1.0f, 0.0f); // Tilt the axis by 45 degrees
    glRotatef(_angle, angle, 1.0f, 0.0f); // Rotate around the inclined axis
    glTranslatef(0.0f, 0.0f, radius); // Move the sphere away from the origin

    // Draw the textured sphere
    GLUquadricObj* quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.5, 20, 20);
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);


    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
    if (key == 'w') {
        eyeY += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 's') {
        eyeY -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'a') {
        eyeX -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'd') {
        eyeX += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'z') {
        eyeZ -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'x') {
        eyeZ += 0.1f;
        glutPostRedisplay();
    }
    else if (key == '0') {
        glEnable(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT2);
        glutPostRedisplay();
    }
    else if (key == '1') {
        initAmbientLight();
        glutPostRedisplay();
    }
    else if (key == '2') {
        initPointLight();
        glutPostRedisplay();
    }
    else if (key == '3') {
        initSpotLight();
        glutPostRedisplay();
    }
    else if (key == '4') {
        initDistantLight();
        glutPostRedisplay();
    }
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w / (float)h, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
}

void timer(int value) {
    if (move == true)
    {
        _angle += 1.0f;
        if (angle >= 360.0f) {
            angle -= 360.0f;
        }
        if (_angle >= 360.0f) {
            _angle -= 360.0f;
        }

        glutPostRedisplay();
    }


    glutTimerFunc(16, timer, 0);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1000, 1000);
    glutCreateWindow("Textured Sphere");
    glEnable(GL_DEPTH_TEST);


    


    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, timer, 0);


    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 0;
}
/*
void initLighting() {
    glEnable(GL_LIGHTING);

    // Ambient light
    GLfloat ambient_light[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);

    // Point light
    GLfloat point_light_position[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Point light position
    glLightfv(GL_LIGHT1, GL_POSITION, point_light_position);
    GLfloat point_light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Point light diffuse color
    glLightfv(GL_LIGHT1, GL_DIFFUSE, point_light_diffuse);

    // Spotlight
    GLfloat spotlight_position[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Spotlight position
    glLightfv(GL_LIGHT2, GL_POSITION, spotlight_position);
    GLfloat spotlight_diffuse[] = { 1.0f, 0.8f, 0.8f, 0.8f }; // Spotlight diffuse color
    glLightfv(GL_LIGHT2, GL_DIFFUSE, spotlight_diffuse);
    GLfloat spot_direction[] = { -1.0f, -1.0f, 0.0f }; // Spotlight direction
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, spot_direction);
    glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 70.0f); // Spotlight cutoff angle
    glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 200.0f); // Spotlight exponent

    // Distant light
    GLfloat distant_light_direction[] = { 0.0f, -1.0f, 0.0f }; // Distant light direction
    glLightfv(GL_LIGHT3, GL_POSITION, distant_light_direction);
    GLfloat distant_light_diffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Distant light diffuse color
    glLightfv(GL_LIGHT3, GL_DIFFUSE, distant_light_diffuse);
}
*/


























//#include<gl/glut.h> 
//#include<stdio.h> 
//using namespace std;
//
//#include <string.h>
//#include <stdlib.h>
//#include <ctime>
//
//#define _USE_MATH_DEFINES
//#include <math.h>
//#include <cmath>
//
//#include "../PTCG_lab4/dependencies/stb/stb_image.h"
//#include "../PTCG_lab4/dependencies/vector3/vector3.h"
//#include "../PTCG_lab4/dependencies/Geometry/Geometry.h"
//
//
//
//
//
//
//
//GLuint textureID;
//void loadTexture(const char* filename)
//{
//	int width, height, nrChannels;
//	unsigned char* data = stbi_load(filename, &width, &height, &nrChannels, 0);
//
//	glGenTextures(0, &textureID);
//	//glBindTexture(GL_TEXTURE_2D, textureID);
//
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//	if (data)
//	{
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
//	}
//	else
//	{
//		std::cout << "Failed to load texture" << std::endl;
//	}
//
//	stbi_image_free(data);
//
//
//
//	/*glEnable(GL_TEXTURE_2D);
//	//AUX_RGBImageRec* texture = auxDIBImageLoad(const char* file);
//	AUX_RGBImageRec* texture = auxDIBImageLoad("");*/
//}
//
//void init() {
//	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//	glEnable(GL_DEPTH_TEST);
//}
//
//void display() {
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(15.0f, 1.0f, 0.1f, 100.0f);
//	gluLookAt(0.0f, 0.0f, 5.0f,
//		0.0f, 0.0f, 0.0f,
//		0.0f, 1.0f, 0.0f);
//
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();	
//
//	glTranslatef(0.0f, 0.0f, -5.0f);
//	glColor3f(1.0f, 1.0f, 1.0f);
//	glutSolidSphere(0.5f, 30, 30); // radius, slices, stacks
//
//	glGetError();
//
//	glutSwapBuffers();
//}
//
//void reshape(int w, int h) {
//	glViewport(0, 0, w, h);
//	glMatrixMode(GL_PROJECTION);
//	glLoadIdentity();
//	gluPerspective(45.0f, (float)w / (float)h, 0.1f, 100.0f);
//	glMatrixMode(GL_MODELVIEW);
//	glLoadIdentity();
//}
//
//int main(int argc, char** argv)
//{
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
//	glutInitWindowSize(800, 800);
//	glutInitWindowPosition(100, 100);
//	glutCreateWindow(argv[0]);
//
//	init();
//	glutDisplayFunc(display);
//	glutReshapeFunc(reshape);
//	//glutKeyboardFunc(keyboard);
//	glutMainLoop();
//	return 0;
//}