#include<stdio.h> 
#include<gl/glut.h> 

#include <string.h>
#include <stdlib.h>
#include <ctime>

#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>






bool ortho = true;
bool wireframe = false;
bool flat = true;
bool dod = true;


// Define the knot vector
GLfloat knotVector[8] = { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0 };

const int numControlPoints = 8;
GLfloat n_ctrlPoints[numControlPoints][4];
//GLfloat n_ctrlPoints[4][4][3];

GLUnurbsObj* theNurb;

void calculateCircleControlPoints(float radius) {
	float angleIncrement = 2.0 * M_PI / (numControlPoints - 1);
	float angle = 0.0;
	for (int i = 0; i < numControlPoints; ++i) {
		n_ctrlPoints[i][0] = radius * cos(angle);  // x-coordinate
		n_ctrlPoints[i][1] = radius * sin(angle);  // y-coordinate
		n_ctrlPoints[i][2] = 0.0;                   // z-coordinate (for 2D)
		n_ctrlPoints[i][3] = 1.0;                   // homogenous coordinate
		angle += angleIncrement;
	}

	//int u, v;
	//for (u = 0; u < 4; u++) {
	//	for (v = 0; v < 4; v++) {
	//		n_ctrlPoints[u][v][0] = ((GLfloat)u - 1.5);
	//		n_ctrlPoints[u][v][1] = ((GLfloat)v - 1.5);

	//		if ((u == 1 || u == 2) && (v == 1 || v == 2))
	//			n_ctrlPoints[u][v][2] = 3.0;
	//		else
	//			n_ctrlPoints[u][v][2] = -3.0;
	//	}
	//}
}

GLfloat ctrlPoints[4][4][3] = {
	/*{{-1.5, -1.5, 4.0}, {-0.5, -1.5, 2.0}, {0.5, -1.5, -1.0}, {1.5, -1.5, 2.0}},
	{ {-1.5, -0.5, 1.0}, {-0.5, -0.5, 3.0}, {0.5, -0.5, 0.0}, {1.5, -0.5, -1.0} },
	{ {-1.5, 0.5, 4.0}, {-0.5, 0.5, 0.0}, {0.5, 0.5, 3.0}, {1.5, 0.5, 4.0} },
	{ {-1.5, 1.5, -2.0}, {-0.5, 1.5, -2.0}, {0.5, 1.5, 0.0}, {1.5, 1.5, -1.0} }*/

	{ {-1.5, -1.5, 0}, {-0.5, -1.5, 0}, {0.5, -1.5, 0}, {1.5, -1.5, 0} },
	{ {-1.5, -0.5, .0}, {-0.5, -0.5, 0}, {0.5, -0.5, 0.0}, {1.5, -0.5, 0} },
	{ {-1.5, 0.5, 0}, {-0.5, 0.5, 0}, {0.5, 0.5, 0}, {1.5, 0.5, 0} },
	{ {-1.5, 1.5, 0}, {-0.5, 1.5, 0}, {0.5, 1.5, 0}, {1.5, 1.5, -0} }
};

void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	// bezier surface
	

	glEnable(GL_MAP2_VERTEX_3);
	glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &ctrlPoints[0][0][0]);

	//
	//glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &ctrlpoints[0][0][0]);
	glEnable(GL_MAP2_VERTEX_3);
	//

	glEnable(GL_DEPTH_TEST);


	// Set up NURBS curve properties
	theNurb = gluNewNurbsRenderer();
	gluNurbsProperty(theNurb, GLU_SAMPLING_TOLERANCE, 25.0);
	gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_OUTLINE_POLYGON);


	float radius = 1.0;
	calculateCircleControlPoints(radius);
}


void display() {

	if(dod)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glShadeModel(flat ? GL_FLAT : GL_SMOOTH);
		glLoadIdentity();

		if (ortho) {
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(-5.0f, 5.0f, -5.0f, 5.0f, -10.0f, 10.0f);
		}
		else
		{
			// perspective
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(15.0f, 1.0f, 0.1f, 100.0f);
			gluLookAt(0.0f, 0.0f, 25.0f,
				0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f);
		}

		// Set up perspective projection
		//glMatrixMode(GL_PROJECTION);
		//glLoadIdentity();
		//gluPerspective(45.0, 1.0, 1.0, 10.0);
		//glMatrixMode(GL_MODELVIEW);



		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// Move the camera back and rotate the dodecahedron
		glTranslatef(0.0, 0.0, -5.0);
		glRotatef(15.0, 1.0, 0.0, 0.0);
		glRotatef(-25.0, 0.0, 1.0, 0.0);

		if (wireframe)
		{
			glutSolidDodecahedron(); // Render the solid dodecahedron
		}
		else
		{
			glutWireDodecahedron();
		}
		glutSwapBuffers();
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glShadeModel(flat ? GL_FLAT : GL_SMOOTH);
		glLoadIdentity();


		if (ortho) {
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(-5.0f, 5.0f, -5.0f, 5.0f, -10.0f, 10.0f);
		}
		else
		{
			// perspective
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(15.0f, 1.0f, 0.1f, 100.0f);
			gluLookAt(0.0f, 0.0f, 25.0f,
				0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f);
		}

		if (wireframe)
		{
			glColor3f(1.0, 1.0, 1.0);
			glPushMatrix();

			// Render the circle
			gluBeginCurve(theNurb);
			gluNurbsCurve(theNurb, 8, knotVector, 3, &n_ctrlPoints[0][0], 4, GL_MAP1_VERTEX_3);
			gluEndCurve(theNurb);

			//gluDeleteNurbsRenderer(theNurb);

			//gluBeginSurface(theNurb);
			//gluNurbsSurface(theNurb, 8, knotVector, 8, knotVector, 4 * 3, 3, &n_ctrlPoints[0][0], 4, 4, GL_MAP1_VERTEX_3);
			//gluEndSurface(theNurb);

			glPopMatrix();



			glColor3f(1.0, 1.0, 1.0);
			glPushMatrix();
			glRotatef(15.0, 15.0, 1.0, 1.0);


			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			int i, j;
			for (j = 0; j <= 8; j++) {
				glBegin(GL_LINE_STRIP);
				for (i = 0; i <= 30; i++) {
					glEvalCoord2f((GLfloat)i / 30.0, (GLfloat)j / 8.0);
				}
				glEnd();
				glBegin(GL_LINE_STRIP);
				for (i = 0; i <= 30; i++) {
					glEvalCoord2f((GLfloat)j / 8.0, (GLfloat)i / 30.0);
				}
				glEnd();
			}


			glPopMatrix();
			glutSwapBuffers();
		}
		else
		{
			glColor3f(0.0, 0.0, 0.0);
			glPushMatrix();

			// Render the circle
			gluBeginCurve(theNurb);
			gluNurbsCurve(theNurb, 8, knotVector, 3, &n_ctrlPoints[0][0], 4, GL_MAP1_VERTEX_3);
			gluEndCurve(theNurb);

			//gluDeleteNurbsRenderer(theNurb);

			//gluBeginSurface(theNurb);
			//gluNurbsSurface(theNurb, 8, knotVector, 8, knotVector, 4 * 3, 3, &n_ctrlPoints[0][0], 4, 4, GL_MAP1_VERTEX_3);
			//gluEndSurface(theNurb);

			glPopMatrix();




			glColor3f(1.0, 1.0, 1.0);
			glPushMatrix();
			glRotatef(15.0, 15.0, 1.0, 1.0);

			glMapGrid2f(30, 0.0, 1.0, 30, 0.0, 1.0);
			glEvalMesh2(GL_FILL, 0, 30, 0, 30);


			glPopMatrix();
			glutSwapBuffers();
		}


		glFlush();
	}
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(ortho)
	{
		if (w <= h)
		glOrtho(-4.0, 4.0, -4.0 * (GLfloat)h / (GLfloat)w, 4.0 * (GLfloat)h / (GLfloat)w, -4.0, 4.0);
	else
		glOrtho(-4.0 * (GLfloat)w / (GLfloat)h, 4.0 * (GLfloat)w / (GLfloat)h, -4.0, 4.0, -4.0, 4.0);
	}
	else
	{
		gluPerspective(45.0, (GLdouble)w / (GLdouble)h, 3.0, 8.0);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -5.0);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 'o':
	case 'O':
		ortho = !ortho;
		glutPostRedisplay();
		break;
	//case 't':
	//case 'T':
	//	teapot = !teapot;
	//	glutPostRedisplay();
	//	break;
	case 'd':
	case 'D':
		dod = !dod;
		glutPostRedisplay();
		break;
	case 'w':
	case 'W':
		wireframe = !wireframe;
		glutPostRedisplay();
		break;
	case 'f':
	case 'F':
		flat = !flat;
		glutPostRedisplay();
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1000, 1000);
	//glutCreateWindow("Dodecahedron");
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	glEnable(GL_DEPTH_TEST);
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}





















































/*
// Define the knot vector
GLfloat knotVector[8] = { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0 };

const int numControlPoints = 8;
GLfloat n_ctrlPoints[numControlPoints][4];
//GLfloat n_ctrlPoints[4][4][3];

GLUnurbsObj* theNurb;

void calculateCircleControlPoints(float radius) {
	float angleIncrement = 2.0 * M_PI / (numControlPoints - 1);
	float angle = 0.0;
	for (int i = 0; i < numControlPoints; ++i) {
		n_ctrlPoints[i][0] = radius * cos(angle);  // x-coordinate
		n_ctrlPoints[i][1] = radius * sin(angle);  // y-coordinate
		n_ctrlPoints[i][2] = 0.0;                   // z-coordinate (for 2D)
		n_ctrlPoints[i][3] = 0.0;                   // homogenous coordinate
		angle += angleIncrement;
	}

	//int u, v;
	//for (u = 0; u < 4; u++) {
	//	for (v = 0; v < 4; v++) {
	//		n_ctrlPoints[u][v][0] = ((GLfloat)u - 1.5);
	//		n_ctrlPoints[u][v][1] = ((GLfloat)v - 1.5);
	//
	//		if ((u == 1 || u == 2) && (v == 1 || v == 2))
	//			n_ctrlPoints[u][v][2] = 3.0;
	//		else
	//			n_ctrlPoints[u][v][2] = -3.0;
	//	}
	//}
}





GLfloat ctrlPoints[4][4][3] = {
	{ {-1.5, -1.5, 4.0}, {-0.5, -1.5, 2.0}, {0.5, -1.5, -1.0}, {1.5, -1.5, 2.0} },
	{ {-1.5, -0.5, 1.0}, {-0.5, -0.5, 3.0}, {0.5, -0.5, 0.0}, {1.5, -0.5, -1.0} },
	{ {-1.5, 0.5, 4.0}, {-0.5, 0.5, 0.0}, {0.5, 0.5, 3.0}, {1.5, 0.5, 4.0} },
	{ {-1.5, 1.5, -2.0}, {-0.5, 1.5, -2.0}, {0.5, 1.5, 0.0}, {1.5, 1.5, -1.0} }
};

void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_FLAT);
	glEnable(GL_MAP2_VERTEX_3);
	glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &ctrlPoints[0][0][0]);	
	glEnable(GL_DEPTH_TEST);


	// Set up NURBS curve properties
	theNurb = gluNewNurbsRenderer();
	gluNurbsProperty(theNurb, GLU_SAMPLING_TOLERANCE, 25.0);
	gluNurbsProperty(theNurb, GLU_DISPLAY_MODE, GLU_OUTLINE_POLYGON);


	float radius = 1.0;
	calculateCircleControlPoints(radius);
}

void display(void) {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();

	// Render the circle
	gluBeginCurve(theNurb);
	gluNurbsCurve(theNurb, 8, knotVector, 3, &n_ctrlPoints[0][0], 4, GL_MAP1_VERTEX_3);
	gluEndCurve(theNurb);

	//gluDeleteNurbsRenderer(theNurb);

	//gluBeginSurface(theNurb);
	//gluNurbsSurface(theNurb, 8, knotVector, 8, knotVector, 4 * 3, 3, &n_ctrlPoints[0][0], 4, 4, GL_MAP1_VERTEX_3);
	//gluEndSurface(theNurb);

	glPopMatrix();




	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	glRotatef(15.0, 15.0, 1.0, 1.0);

	int i, j;
	for (j = 0; j <= 8; j++) {
		glBegin(GL_LINE_STRIP);
		for (i = 0; i <= 30; i++) {
			glEvalCoord2f((GLfloat)i / 30.0, (GLfloat)j / 8.0);
		}
		glEnd();
		glBegin(GL_LINE_STRIP);
		for (i = 0; i <= 30; i++) {
			glEvalCoord2f((GLfloat)j / 8.0, (GLfloat)i / 30.0);
		}
		glEnd();
	}

	glPopMatrix();
	glutSwapBuffers();




	
	

	glFlush();

}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
		glOrtho(-4.0, 4.0, -4.0 * (GLfloat)h / (GLfloat)w, 4.0 * (GLfloat)h / (GLfloat)w, -4.0, 4.0);
	else
		glOrtho(-4.0 * (GLfloat)w / (GLfloat)h, 4.0 * (GLfloat)w / (GLfloat)h, -4.0, 4.0, -4.0, 4.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutMainLoop();
	return 0;
}*/








/*void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	// Set up orthogonal projection
	glOrtho(-2.0, 2.0, -2.0, 2.0, -10.0, 10.0);

	glRotatef(30.0, 1.0, 0.0, 0.0); // Rotate the dodecahedron
	glRotatef(-30.0, 0.0, 1.0, 0.0);
	glutSolidDodecahedron(); // Render the solid dodecahedron
	glutSwapBuffers();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Dodecahedron");
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}*/




/*bool ortho = true;
bool cube = true;
bool wireframe = true;
bool flat = true;

GLdouble size = 5.0;

GLfloat controlPoints[4][4][3];
GLfloat knots[8] = { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0 };
GLfloat edgePoints[5][2] = { {0.0, 0.0}, {1.0, 0.0}, {1.0, 1.0}, {0.0, 1.0}, {0.0, 0.0} }; // there counter clockwise 
GLfloat edgePointsPentagon[6][2] = { {0.8, 0.5}, {0.8, 0.1}, {0.1, 0.1}, {0.1, 0.5}, {0.5, 0.8}, {0.8, 0.5} }; // and there clockwise 

GLUnurbsObj* nurbRenderer;

void init_surface(void)
{
	int u, v;
	for (u = 0; u < 4; u++) {
		for (v = 0; v < 4; v++) {
			controlPoints[u][v][0] = 2.0 * ((GLfloat)u - 1.5);
			controlPoints[u][v][1] = 2.0 * ((GLfloat)v - 1.5);

			if ((u == 1 || u == 2) && (v == 1 || v == 2))
				controlPoints[u][v][2] = 3.0;
			else
				controlPoints[u][v][2] = -3.0;
		}
	}
}

void init()
{
	GLfloat mat_diffuse[] = { 0.7, 0.7, 0.7, 1.0 };
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 100.0 };

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	init_surface();

	nurbRenderer = gluNewNurbsRenderer();
	gluNurbsProperty(nurbRenderer, GLU_SAMPLING_TOLERANCE, 25.0);
	gluNurbsProperty(nurbRenderer, GLU_DISPLAY_MODE, GLU_FILL);
}

void displayCube()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (ortho) {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-15.0f, 15.0f, -15.0f, 15.0f, -15.0f, 15.0f);
	}
	else
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(60.0f, 1.0f, 0.1f, 100.0f);
		gluLookAt(0.0f, 0.0f, 50.0f,
			0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f);
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glTranslatef(0.0, 0.0, -4.5);
	glScalef(1.0, 1.0, 1.0);
	glColor3f(0.5f, 0.0f, 0.8f);

	if (wireframe)
		glutWireCube(size); // Draw a wireframe cube with a side length of 2.0
	else
		glutWireTeapot(size);

	glPopMatrix();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glShadeModel(flat ? GL_FLAT : GL_SMOOTH);

	if (cube)
		displayCube();
	else
		//displaySurface();

	glFlush();
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (GLdouble)w / (GLdouble)h, 3.0, 8.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -5.0);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 'o':
	case 'O':
		ortho = !ortho;
		glutPostRedisplay();
		break;
	case 't':
	case 'T':
		cube = !cube;
		glutPostRedisplay();
		break;
	case 'w':
	case 'W':
		wireframe = !wireframe;
		glutPostRedisplay();
		break;
	case 'f':
	case 'F':
		flat = !flat;
		glutPostRedisplay();
		break;
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMainLoop();
	return 0;
}*/


































/*int faces[12][5] =
{
	{0, 16, 2, 10, 8},
	{0, 8, 4, 14, 12},
	{16, 17, 1, 12, 0},
	{1, 9, 11, 3, 17},
	{1, 12, 14, 5, 9},
	{2, 13, 15, 6, 10},
	{13, 3, 17, 16, 2},
	{3, 11, 7, 15, 13},
	{4, 8, 10, 6, 18},
	{14, 5, 19, 18, 4},
	{5, 19, 7, 11, 9},
	{15, 7, 19, 18, 6}
};
double points[20][3] =
{
	{1, 1, 1},
	{1, 1, -1},
	{1, -1, 1},
	{1, -1, -1},
	{-1, 1, 1},
	{-1, 1, -1},
	{-1, -1, 1},
	{-1, -1, -1},
	{0, 0.618, 1.618},
	{0, 0.618, -1.618},
	{0, -0.618, 1.618},
	{0, -0.618, -1.618},
	{0.618, 1.618, 0},
	{0.618, -1.618, 0},
	{-0.618, 1.618, 0},
	{-0.618, -1.618, 0},
	{1.618, 0, 0.618},
	{1.618, 0, -0.618},
	{-1.618, 0, 0.618},
	{-1.618, 0, -0.618}
};*/
/*void cross(double a[], double b[], double vec1[])
{
	vec1[0] = a[1] * b[2] - a[2] * b[1];
	vec1[1] = a[2] * b[0] - a[0] * b[2];
	vec1[2] = a[0] * b[1] - a[1] * b[0];
}
void getNormal(int n, int m, int x, double vec1[])
{
	double a[] = { points[m][0] - points[n][0], points[m][1] - points[n][1], points[m][2] - points[n][2] };
	double b[] = { points[m][0] - points[x][0], points[m][1] - points[x][1], points[m][2] - points[x][2] };
	cross(a, b, vec1);
}
double dot(double a[], double b[])
{
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}
double mag(double a[])
{
	return sqrt(pow(a[0], 2) + pow(a[1], 2) + pow(a[2], 2));
}*/

/*void myFunc(unsigned char key, int x, int y) {
	if (key == 'w' || key == 'W')
		glRotated(10.0, 0.0, 0.0, 1.0);
	else if (key == 'a' || key == 'A')
		glRotated(10.0, 0.0, -1.0, 0.0);
	else if (key == 's' || key == 'S')
		glRotated(10.0, 0.0, 0.0, -1.0);
	else if (key == 'd' || key == 'D')
		glRotated(10.0, 0.0, 1.0, 0.0);
	glutPostRedisplay();
}
void reshape(int w, int h) {
	double aspect = double(w) / double(h);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glOrtho(-3.0 * aspect, 3.0 * aspect, -3.0, 3.0, -3.0, 3.0);
	glMatrixMode(GL_MODELVIEW);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (int i = 0; i < 12; i++)
	{
		glColor3f(0.0, 0.1, 0.1);
		glBegin(GL_TRIANGLE_FAN);
		for (int x = 0; x < 5; x++)
			glVertex3dv(points[faces[i][x]]);
		glEnd();
		glColor3f(0.0, 1.0, 1.0);
		glBegin(GL_LINE_STRIP);
		for (int x = 0; x < 5; x++)
			glVertex3dv(points[faces[i][x]]);
		glVertex3dv(points[faces[i][0]]);
		glEnd();
	}
	glFlush();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Dodecahedron");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(myFunc);
	glutMainLoop();
	return 0;
}*/
















/*float rotationAngle = 0.0;

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix(); // Save the current matrix

    //glRotatef(rotationAngle, 1.0, 1.0, 1.0); // Rotate around the x, y, and z axes

    // Draw cube
    glBegin(GL_QUADS);

    // Front face
    glColor3f(1.0, 0.0, 0.0); // Red
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);

    // Back face
    glColor3f(0.0, 1.0, 0.0); // Green
    glVertex3f(-0.5, -0.5, -0.5);
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(0.5, -0.5, -0.5);

    // Top face
    glColor3f(0.0, 0.0, 1.0); // Blue
    glVertex3f(-0.5, 0.5, -0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, 0.5, -0.5);

    // Bottom face
    glColor3f(1.0, 1.0, 0.0); // Yellow
    glVertex3f(-0.5, -0.5, -0.5);
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(-0.5, -0.5, 0.5);

    // Right face
    glColor3f(1.0, 0.0, 1.0); // Magenta
    glVertex3f(0.5, -0.5, -0.5);
    glVertex3f(0.5, 0.5, -0.5);
    glVertex3f(0.5, 0.5, 0.5);
    glVertex3f(0.5, -0.5, 0.5);

    // Left face
    glColor3f(0.0, 1.0, 1.0); // Cyan
    glVertex3f(-0.5, -0.5, -0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(-0.5, 0.5, 0.5);
    glVertex3f(-0.5, 0.5, -0.5);

    glEnd();

    glPopMatrix(); // Restore the previous matrix

    glutSwapBuffers();
}

void init() {
    glClearColor(1.0, 1.0, 1.0, 1.0); // Set background color to white
    glEnable(GL_DEPTH_TEST); // Enable depth testing for 3D rendering
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, 1.0, 0.1, 10.0); // Set up perspective projection
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(1.0, 1.0, 3.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0); // Set up viewing transformation
}

void update(int value) {
    rotationAngle += 2.0; // Increment rotation angle
    if (rotationAngle > 360) {
        rotationAngle -= 360; // Keep angle within 0-360 range
    }
    glutPostRedisplay(); // Call display function
    glutTimerFunc(25, update, 0); // Update every 25 milliseconds
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutCreateWindow("OpenGL Cube");

    init();
    glutDisplayFunc(display);
    //glutTimerFunc(25, update, 0); // Start the update loop
    glutMainLoop();

    return 0;
}*/
