#include<stdio.h> 
#include<gl/glut.h> 
#include<math.h> 
#include <string.h>
#include <stdlib.h>
#include <ctime>

float rotationAngle = 0.0;
float pivotX = 0.5;
float pivotY = 0;

float scaleFactor = 0.1;
bool scaleIncreaseWay = true;

float xy[6][2];

void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();

        
        // Translate to the pivot point
        glTranslatef(100 * scaleFactor, 100 * scaleFactor, 0.0);

        // Rotate around the pivot point
        glRotatef(rotationAngle, 0.0, 0.0, 1.0);

        // Translate back to the original position
        glTranslatef(-100 * scaleFactor, -100 * scaleFactor, 0.0);



        glScalef(scaleFactor, scaleFactor, 1.0);

        // Draw square
        glBegin(GL_POLYGON);
        glColor3f(0.0, 0.0, 0.0); // Set color
        for (float angle = 0, i = 0; angle < 2 * 3.14159; angle += 3.14159 / 3, i++) {
            float v1 = 0 + 100 * cos(angle), v2 = 0 + 100 * sin(angle);
            xy[(int)i][0] = v1, xy[(int)i][1] = v2;
            glVertex2f(v1, v2);
        }
        glEnd();
    
    glPopMatrix();

    glFlush();
}

void update(int value) {
    rotationAngle += 5.0; // Increment rotation angle
    if (rotationAngle > 360) {
        rotationAngle -= 360; // Keep angle within 0-360 range
    }
    
    if (scaleIncreaseWay) {
        scaleFactor += 0.1; // Increase scale factor
        if (scaleFactor > 2) 
        {
            scaleIncreaseWay = false;
        }
    }
    else
    {
        scaleFactor -= 0.1;
        if (scaleFactor < 0.1) 
        {
            scaleIncreaseWay = true;
        }
    }

    glutPostRedisplay(); // Call display function
    glutTimerFunc(250, update, 0); // Update every 25 milliseconds
}

void init() {
    glClearColor(1.0, 1.0, 1.0, 1.0); // Set background color to white
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1000.0, 1000.0, -1000.0, 1000.0); // Set up 2D orthographic projection
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1000, 1000); // Set window size
    glutCreateWindow("OpenGL Square and Triangle");

    init();
    glutDisplayFunc(display);
    glutTimerFunc(250, update, 0); // Start the update loop
    glutMainLoop();

    return 0;
}










/*void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    for (int i = 0; i < 6; i++)
    {
        glPushMatrix();

        // Translate to the pivot point
        glTranslatef(xy[i][0] + 100, xy[i][1], 0.0);


        // Rotate around the pivot point
        glRotatef(rotationAngle, 0, 0, 1.0);


        // Translate back to the original position
        glTranslatef((-1) * xy[i][0] - 100, (-1) * xy[i][1], 0.0);

        // Draw square
        glBegin(GL_POLYGON);
        glColor3f(0.0, 0.0, 0.0); // Set color
        for (float angle = 0; angle < 2 * 3.14159; angle += 3.14159 / 3) {
            float v1 = xy[i][0] + 100 * cos(angle), v2 = xy[i][1] + 100 * sin(angle);
            glVertex2f(v1, v2);
        }
        glEnd();

        glPopMatrix();
    }

    glFlush();
}*/