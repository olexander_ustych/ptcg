#include<gl/glut.h> 
#include <GL/freeglut.h>
#include <cstdio>
#include <SOIL.h>

float eyeX = 10.0f;
float eyeY = -10.0f;
float eyeZ = 0.0f;
float centerX = 0.0f;
float centerY = 0.0f;
float centerZ = 0.0f;
float upX = 0.0f;
float upY = 1.0f;
float upZ = 0.0f;

float angle = 45.0f, _angle = 0;
float radius = 5.0f;

bool move = true;

bool isFullScreen = false;
float objectPosition[] = { 0, 0, 0 };
float moveVector[] = { 0.005,0.01,0.012 };

// Load image data into an OpenGL texture
GLuint LoadTexture(const char* filename) {
    // Code to load image data from file and create an OpenGL texture

    GLuint textureId = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_TEXTURE_REPEATS);
    if (textureId == 0) {
        printf("Error loading texture: %s\n", SOIL_last_result());
    }

    return textureId;
}

// Set the ambient lighting properties
GLfloat ambientLight[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Low ambient light level

// Set the material properties
GLfloat materialColor[] = { 0.8f, 0.8f, 0.8f, 1.0f }; // Object color
GLfloat materialAmbient[] = { 0.5f, 0.5f, 0.5f, 1.0f }; // Object ambient reflection

void initAmbientLight() {
    glEnable(GL_LIGHTING);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Set the background color
    glShadeModel(GL_SMOOTH); // Enable smooth shading

    // Set the ambient light properties
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

    // Set the material properties
    glMaterialfv(GL_FRONT, GL_DIFFUSE, materialColor);
    glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
}

void initPointLight() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // Point light
    GLfloat light_position[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light_diffuse[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}

void initSpotLight() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);

    // Spotlight
    GLfloat spotlight_position[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Spotlight position
    glLightfv(GL_LIGHT1, GL_POSITION, spotlight_position);
    GLfloat spotlight_diffuse[] = { 0.0f, 1.0f, 0.0f, 1.0f }; // Spotlight diffuse color
    glLightfv(GL_LIGHT1, GL_DIFFUSE, spotlight_diffuse);
    GLfloat spot_direction[] = { -1.0f, -1.0f, 0.0f }; // Spotlight direction
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 70.0f); // Spotlight cutoff angle
    glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 200.0f); // Spotlight exponent

    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}

void initDistantLight() {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT2);

    GLfloat distant_light_direction[] = { 100.0f, 100.0f, 100.0f }; // Distant light direction
    glLightfv(GL_LIGHT2, GL_POSITION, distant_light_direction);
    GLfloat distant_light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f }; // Distant light diffuse color
    glLightfv(GL_LIGHT2, GL_DIFFUSE, distant_light_diffuse);
    glLightfv(GL_LIGHT2, GL_SPECULAR, distant_light_diffuse);//���� ���������

    GLfloat linearAttenuation = 0.05f;
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
}


void changeMoveVector()
{
    if (((objectPosition[0] >= 4 || objectPosition[0] <= -4) && !isFullScreen) || ((objectPosition[0] >= 8 || objectPosition[0] <= -8) && isFullScreen))
    {
        moveVector[0] = -moveVector[0];
    }
    if (((objectPosition[1] >= 3 || objectPosition[1] <= -3) && !isFullScreen) || ((objectPosition[1] >= 8 || objectPosition[1] <= -8) && isFullScreen))
    {
        moveVector[1] = -moveVector[1];
    }
    if (objectPosition[2] >= 8 || objectPosition[2] <= -8)
    {
        moveVector[2] = -moveVector[2];
    }
}

// Render a textured sphere
void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 2, 2, 100);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);


    /*
    glEnable(GL_FOG);
    GLfloat fogColor[] = { 0.5f, 0.5f, 1.0f, 1.0f }; // ���� ������ (����)
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_MODE, GL_EXP); // ����� ������: ��������������
    glFogf(GL_FOG_DENSITY, 0.1f); // ������� ������
    */


    // Load the texture
    GLuint textureId = LoadTexture("My_profile.png");

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId);

    // Set up texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glPushMatrix();

    glRotatef(0.0f, angle, 1.0f, 0.0f); // Tilt the axis by 45 degrees
    glRotatef(_angle, angle, 1.0f, 0.0f); // Rotate around the inclined axis
    //glTranslatef(0.0f, 0.0f, radius); // Move the sphere away from the origin

    //
    glTranslatef(objectPosition[0], objectPosition[1], objectPosition[2]);
    objectPosition[0] += moveVector[0];
    objectPosition[1] += moveVector[1];
    objectPosition[2] += moveVector[2];
    //

    // Draw the textured sphere
    GLUquadricObj* quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 0.5, 20, 20);
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);

    changeMoveVector();

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
    if (key == 'w') {
        eyeY += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 's') {
        eyeY -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'a') {
        eyeX -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'd') {
        eyeX += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'z') {
        eyeZ -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'x') {
        eyeZ += 0.1f;
        glutPostRedisplay();
    }
    else if (key == '0') {
        glEnable(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT2);
        glutPostRedisplay();
    }
    else if (key == '1') {
        initAmbientLight();
        glutPostRedisplay();
    }
    else if (key == '2') {
        initPointLight();
        glutPostRedisplay();
    }
    else if (key == '3') {
        initSpotLight();
        glutPostRedisplay();
    }
    else if (key == '4') {
        initDistantLight();
        glutPostRedisplay();
    }
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w / (float)h, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
}

void timer(int value) {
    if (move == true)
    {
        _angle += 1.0f;
        if (angle >= 360.0f) {
            angle -= 360.0f;
        }
        if (_angle >= 360.0f) {
            _angle -= 360.0f;
        }    

        glutPostRedisplay();
    }


    glutTimerFunc(16, timer, 0);
}

void handleFogMenu(int option)
{
    switch (option)
    {
    case 1:
        glEnable(GL_FOG);        
        glFogi(GL_FOG_MODE, GL_LINEAR);
        glFogf(GL_FOG_START, 1.0);
        glFogf(GL_FOG_END, 7.0);
        break;
    case 2:
        glEnable(GL_FOG);        
        glFogi(GL_FOG_MODE, GL_EXP);
        glFogf(GL_FOG_DENSITY, 0.2);
        break;
    case 3:
        glDisable(GL_FOG);
        break;
    }
}

void handleLightMenu(int option)
{   
    switch (option)
    {
    case 0:
        glEnable(GL_LIGHTING);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT2);
        glutPostRedisplay();
        break;
    case 1:
        initAmbientLight();
        glutPostRedisplay();
        break;
    case 2:
        initPointLight();
        glutPostRedisplay();
        break;
    case 3:
        initSpotLight();
        glutPostRedisplay();
        break;
    case 4:
        initDistantLight();
        glutPostRedisplay();
        break;
    }
}

void handleFullScreen(int option)
{
    switch (option)
    {
    case 1:
        isFullScreen = true;
        glutFullScreen();
        break;
    case 2:
        isFullScreen = false;        
        glutLeaveFullScreen();
        break;
    }
}

void handleMoveSpeed(int option)
{
    switch (option)
    {
    case 1:
        objectPosition[0] = 0;
        objectPosition[1] = 0;
        objectPosition[2] = 0;

        moveVector[0] *= 3;
        moveVector[1] *= 3;
        moveVector[2] *= 3;
        break;
    case 2:
        objectPosition[0] = 0;
        objectPosition[1] = 0;
        objectPosition[2] = 0;

        moveVector[0] /= 3;
        moveVector[1] /= 3;
        moveVector[2] /= 3;
        break;
    }
}

void handleRotateSpeed(int option)
{
    switch (option)
    {
    case 1:
        angle += 0.5f;
        break;
    case 2:
        angle -= 0.5f;
        break;
    }
}

void createMenu()
{
    int fogMenu = glutCreateMenu(handleFogMenu);
    glutAddMenuEntry("Enable linear fog", 1);
    glutAddMenuEntry("Enable exponential fog", 2);
    glutAddMenuEntry("Disable the fog", 3);

    int lightMenu = glutCreateMenu(handleLightMenu);
    glutAddMenuEntry("Disable light", 0);
    glutAddMenuEntry("Ambient light", 1);
    glutAddMenuEntry("Point light", 2);
    glutAddMenuEntry("Spot light", 3);
    glutAddMenuEntry("Distant light", 4);


    int moveMenu = glutCreateMenu(handleMoveSpeed);
    glutAddMenuEntry("Increase", 1);
    glutAddMenuEntry("Decrease", 2);

    int rotateMenu = glutCreateMenu(handleRotateSpeed);
    glutAddMenuEntry("Increase", 1);
    glutAddMenuEntry("Decrease", 2);

    int fullScreenMenu = glutCreateMenu(handleFullScreen);
    glutAddMenuEntry("On full screen", 1);
    glutAddMenuEntry("Off full screen", 2);

    glutCreateMenu([](int) {}); // ������ �������� ��� ���������� ����
    glutAddSubMenu("Fog", fogMenu);
    glutAddSubMenu("Light", lightMenu);
    glutAddSubMenu("Move", moveMenu);
    glutAddSubMenu("Rotate", rotateMenu);
    glutAddSubMenu("FullScreen", fullScreenMenu);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1000, 1000);
    glutCreateWindow("Textured Sphere");
    glEnable(GL_DEPTH_TEST);





    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, timer, 0);
    createMenu();


    glEnable(GL_DEPTH_TEST);

    glutMainLoop();
    return 0;
}