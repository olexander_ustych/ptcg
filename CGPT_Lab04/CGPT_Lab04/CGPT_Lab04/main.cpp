#include <GL/glut.h>
#include <GL/freeglut.h>
#include "Pyramid/Pyramid.h"
#include "Vector/vector3.h"

void timer(int value);
void display();
void init();
void resize(int w, int h);
void handleKeypress(unsigned char key, int x, int y);
void createMenu();
void changeMoveVector();

const int FOG_DISABLE = 0;
const int FOG_LINEAR = 1;
const int FOG_EXPONENTIAL = 2;

int fogMode = FOG_LINEAR;

const int POINT_LIGHT = 0;
const int SPOT_LIGHT = 1;
const int DISTANT_LIGHT = 2;

int lightMode = 1;

Pyramid* pyramid = NULL;
float angle = 1;

bool isFullScreen = false;
float objectPosition[] = { 0, 0, 0 };
float moveVector[] = { 0.005,0.01,0.012 };

void changeMoveVector()
{
	if (((objectPosition[0] >= 8 || objectPosition[0] <= -8) && !isFullScreen) || ((objectPosition[0] >= 12 || objectPosition[0] <= -12) && isFullScreen))
	{
		moveVector[0] = -moveVector[0];
	}
	if (((objectPosition[1] >= 8 || objectPosition[1] <= -8) && !isFullScreen) || ((objectPosition[1] >= 12 || objectPosition[1] <= -12) && isFullScreen))
	{
		moveVector[1] = -moveVector[1];
	}
	if (objectPosition[2] >= 12 || objectPosition[2] <= -12)
	{
		moveVector[2] = -moveVector[2];
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutCreateWindow("Lab 04");
	init();
	glutTimerFunc(0, timer, 0);
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(handleKeypress);
	createMenu();

	glutMainLoop();
	return 0;
}

void timer(int value)
{
	if (pyramid)
		pyramid->increaseAngle(angle);

	glutPostRedisplay();
	glutTimerFunc(16, timer, 0);
}

void handleLight()
{
	glEnable(GL_LIGHTING);
	float brightness = 0.5f;
	GLfloat ambientLight[] = { brightness, brightness, brightness, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);//������ ���������
	
	if (lightMode == POINT_LIGHT) //������� ���������
	{
		glDisable(GL_LIGHT1);
		glDisable(GL_LIGHT2);

		glEnable(GL_LIGHT0);
		GLfloat lightPos[] = { -0.4f, 1.0f, -6.2f, 1.0f };
		GLfloat lightColor[] = { 1, 0.0, 0.0, 1.0f };
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

		GLfloat linearAttenuation = 0.05f;
		glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
	}
	else if (lightMode == SPOT_LIGHT)//���������
	{
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT2);

		glEnable(GL_LIGHT1);
		GLfloat lightPos[] = { -6.0f, 1.0f, 14.0f, 1.0f };
		GLfloat spotDirection[] = { 1.0f, -0.5f, -2.0f };
		GLfloat lightColor[] = { 0, 1, 0.0, 1.0f };

		glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor); //���� ���������� (������� �������) ��� ������� �����
		glLightfv(GL_LIGHT1, GL_POSITION, lightPos); //������� ������� �����
		glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0f); //��� �������� ���������� ��� ������� �����
		glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotDirection); //�������� ���������� ��� ������� �����

		GLfloat linearAttenuation = 0.05f;
		glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, linearAttenuation);//�������� ���������
	}
	else if (lightMode == DISTANT_LIGHT) //�������� ���������
	{
		glDisable(GL_LIGHT0);
		glDisable(GL_LIGHT1);

		glEnable(GL_LIGHT2);
		GLfloat lightPos[] = { 0.0f, 3.0f, 0.8f, 0.0 };
		GLfloat lightColor[] = { 1, 0, 1, 1.0f };
		glLightfv(GL_LIGHT2, GL_SPECULAR, lightColor);//���� ���������
		glLightfv(GL_LIGHT2, GL_DIFFUSE, lightColor);
		glLightfv(GL_LIGHT2, GL_POSITION, lightPos);

		GLfloat linearAttenuation = 0.05f;
		glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, linearAttenuation); //�������� ���������
	}
}



void displayPiramide()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//camera setup
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, 1.0f, 0.1f, 100.0f);
	gluLookAt(0.0f, 0.0f, 25.0f,
		0, 0, 0,
		0, 1, 0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glTranslatef(objectPosition[0], objectPosition[1], objectPosition[2]);
	objectPosition[0] += moveVector[0];
	objectPosition[1] += moveVector[1];
	objectPosition[2] += moveVector[2];

	pyramid->draw();
	glPopMatrix();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0, 0, -20);
	handleLight();

	displayPiramide();
	changeMoveVector();
	glutSwapBuffers();
}

void init()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_COLOR_MATERIAL);
	//������� ������� ������
	GLfloat fogColor[4] = { 0.5, 0.5, 0.5, 1.0 };
	glFogfv(GL_FOG_COLOR, fogColor);

	//��������� ������
	pyramid = new Pyramid();
}

void resize(int width, int height)
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (float)width / (float)height, 2, 50);

	glMatrixMode(GL_MODELVIEW);
}

void handleKeypress(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'f':
			if (fogMode == FOG_DISABLE)
			{
				glEnable(GL_FOG);
				fogMode = FOG_LINEAR;
				//������ ������� ��������� ������
				glFogi(GL_FOG_MODE, GL_LINEAR);
				glFogf(GL_FOG_START, 1.0); // ������������ ��������� ����� ������
				glFogf(GL_FOG_END, 7.0); // ������������ ������ ����� ������
			}
			else if (fogMode == FOG_LINEAR)
			{
				//������������� ������� ��������� ������
				fogMode = FOG_EXPONENTIAL;

				glFogi(GL_FOG_MODE, GL_EXP);
				glFogf(GL_FOG_DENSITY, 0.3);//���������� ������� ������
			}
			else
			{
				//��������� ������
				fogMode = FOG_DISABLE;
				glDisable(GL_FOG);
			}
			break;
		case 'p':
			if (angle == 0)
			{
				angle = 1;
			}
			else angle = 0;
		case'1':
			lightMode = POINT_LIGHT;
			break;
		case'2':
			lightMode = SPOT_LIGHT;
			break;
		case'3':
			lightMode = DISTANT_LIGHT;
			break;
	}
}

void handleFogMenu(int option)
{
	switch (option)
	{
	case 1:
		glEnable(GL_FOG);
		fogMode = FOG_LINEAR;
		glFogi(GL_FOG_MODE, GL_LINEAR);
		glFogf(GL_FOG_START, 1.0);
		glFogf(GL_FOG_END, 7.0);
		break;
	case 2:
		glEnable(GL_FOG);
		fogMode = FOG_EXPONENTIAL;
		glFogi(GL_FOG_MODE, GL_EXP);
		glFogf(GL_FOG_DENSITY, 0.2);
		break;
	case 3:
		fogMode = FOG_DISABLE;
		glDisable(GL_FOG);
		break;
	}
}

void handleLightMenu(int option)
{
	switch (option)
	{
	case 1:
		lightMode = POINT_LIGHT;
		break;
	case 2:
		lightMode = SPOT_LIGHT;
		break;
	case 3:
		lightMode = DISTANT_LIGHT;
		break;
	}
}

void handleFullScreen(int option)
{
	switch (option)
	{
	case 1:
		isFullScreen = true;
		glutFullScreen();
		break;
	case 2:
		isFullScreen = false;
		/*glutReshapeWindow(1200, 900);
		glutPositionWindow(0, 0);*/
		glutLeaveFullScreen();
		break;
	}
}

void handleMoveSpeed(int option)
{
	switch (option)
	{
	case 1:
		objectPosition[0] = 0;
		objectPosition[1] = 0;
		objectPosition[2] = 0;

		moveVector[0] *= 3;
		moveVector[1] *= 3;
		moveVector[2] *= 3;
		break;
	case 2:
		objectPosition[0] = 0;
		objectPosition[1] = 0;
		objectPosition[2] = 0;

		moveVector[0] /= 3;
		moveVector[1] /= 3;
		moveVector[2] /= 3;
		break;
	}
}

void handleRotateSpeed(int option)
{
	switch (option)
	{
	case 1:
		angle += 0.5f;
		break;
	case 2:
		angle -= 0.5f;
		break;
	}
}

void createMenu()
{
	int fogMenu = glutCreateMenu(handleFogMenu);
	glutAddMenuEntry("Enable linear fog", 1);
	glutAddMenuEntry("Enable exponential fog", 2);
	glutAddMenuEntry("Disable the fog", 3);

	int lightMenu = glutCreateMenu(handleLightMenu);
	glutAddMenuEntry("Point light", 1);
	glutAddMenuEntry("Spot light", 2);
	glutAddMenuEntry("Distant light", 3);


	int moveMenu = glutCreateMenu(handleMoveSpeed);
	glutAddMenuEntry("Increase", 1);
	glutAddMenuEntry("Decrease", 2);

	int rotateMenu = glutCreateMenu(handleRotateSpeed);
	glutAddMenuEntry("Increase", 1);
	glutAddMenuEntry("Decrease", 2);

	int fullScreenMenu = glutCreateMenu(handleFullScreen);
	glutAddMenuEntry("On full screen", 1);
	glutAddMenuEntry("Off full screen", 2);

	glutCreateMenu([](int) {}); // ������ �������� ��� ���������� ����
	glutAddSubMenu("Fog", fogMenu);
	glutAddSubMenu("Light", lightMenu);
	glutAddSubMenu("Move", moveMenu);
	glutAddSubMenu("Rotate", rotateMenu);
	glutAddSubMenu("FullScreen", fullScreenMenu);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}