#pragma once
#define _USE_MATH_DEFINES
#include <GL/glut.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <cmath>
#include <iostream>
class Pyramid
{
private:
	//������� ������
	GLfloat vertices[5][3] = {
		{0, 1, 0},
		{-1, -1, 1},
		{1, -1, 1},
		{1, -1, -1},
		{-1, -1, -1}};
	//������� ������ �� ��������� ���������
	GLuint indices[4][3] = {
		{0, 1, 2},
		{0, 2, 3},
		{0, 3, 4},
		{0, 4, 1}
	};

	//������� ������
	GLfloat colors[4][3] = {
		{1, 0, 0},
		{0, 1, 0},
		{0, 0, 1},
		{1, 1, 0}
	};

	float angle = 0;
	GLuint texture;

	void rotate()
	{
		glTranslatef(vertices[0][0], vertices[0][1], vertices[0][2]);
		glRotatef(angle, vertices[1][0] - vertices[0][0],
			vertices[1][1] - vertices[0][1], vertices[1][2] - vertices[0][2]);
		glTranslatef(-vertices[0][0], -vertices[0][1], -vertices[0][2]);
	}
	void loadTexture(const char* imagepath)
	{
		int width, height, nrChannels;
		unsigned char* data = stbi_load(imagepath, &width, &height, &nrChannels, 0);
		if (data)
		{
			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);
	}
public:
	Pyramid()
	{
		loadTexture("E://Study//Lab_ZPCG//CGPT_Lab04//CGPT_Lab04//Images//texture.png");
	}

	void draw()
	{
		rotate();

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture);

		glBegin(GL_TRIANGLES);
		for (int i = 0; i < 4; i++)
		{
			glColor3f(colors[i][0], colors[i][1], colors[i][2]);
			glVertex3fv(vertices[indices[i][0]]);
			glVertex3fv(vertices[indices[i][1]]);
			glVertex3fv(vertices[indices[i][2]]);
		}
		glEnd();

		glColor3f(1, 1, 1);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex3fv(vertices[1]);
		glTexCoord2f(1.0f, 0.0f); glVertex3fv(vertices[2]);
		glTexCoord2f(1.0f, 1.0f); glVertex3fv(vertices[3]);
		glTexCoord2f(0.0f, 1.0f); glVertex3fv(vertices[4]);
		glEnd();
	}

	void increaseAngle(float value)
	{
		angle += value;

		if (angle > 360)
		{
			angle -= 360;
		}
	}
};