#include <glut.h>

bool isPerspective = true;
bool isBezier = false;
bool smoothShading = false;


float eyeX = 0.0f;
float eyeY = 0.0f;
float eyeZ = 5.0f;
float centerX = 0.0f;
float centerY = 0.0f;
float centerZ = 0.0f;
float upX = 0.0f;
float upY = 1.0f;
float upZ = 0.0f;

GLfloat ctrlpoints[4][4][3] = {
    { { -1.5, -1.5, 0.1 }, { -0.5, -1.5, 0.4 }, { 0.5, -1.5, 0.7 }, { 1.5, -1.5, 1.0 } },
    { { -1.5, -0.5, 0.2 }, { -0.5, -0.5, 0.5 }, { 0.5, -0.5, 0.8 }, { 1.5, -0.5, 1.1 } },
    { { -1.5, 0.5, 0.3 }, { -0.5, 0.5, 0.6 }, { 0.5, 0.5, 0.9 }, { 1.5, 0.5, 1.2 } },
    { { -1.5, 1.5, 0.4 }, { -0.5, 1.5, -0.7 }, { 0.5, 1.5, 1.0 }, { 1.5, 1.5, -1.3 } }
};

void drawBezier()
{
    // �������� ���������� � ����� �����
    GLfloat x = 0.5;
    GLfloat y = 0.0;
    GLfloat z = 0.7;
    GLfloat width = 1.0;
    GLfloat height = 1.0;
    GLfloat depth = 0.5;

    if (smoothShading) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // ����� �������
    }
    else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // ����� ������������� ����������
    }


    GLfloat material_diffuse_black[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_black);

    // Front face
    glBegin(GL_QUADS);
    glVertex3f(x - width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z - depth / 2);
    glVertex3f(x - width / 2, y + height / 2, z - depth / 2);
    glEnd();

    // Back face
    glBegin(GL_QUADS);
    glVertex3f(x - width / 2, y - height / 2, z + depth / 2);
    glVertex3f(x + width / 2, y - height / 2, z + depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z + depth / 2);
    glVertex3f(x - width / 2, y + height / 2, z + depth / 2);
    glEnd();

    // Left face
    glBegin(GL_QUADS);
    glVertex3f(x - width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x - width / 2, y - height / 2, z + depth / 2);
    glVertex3f(x - width / 2, y + height / 2, z + depth / 2);
    glVertex3f(x - width / 2, y + height / 2, z - depth / 2);
    glEnd();

    // Right face
    glBegin(GL_QUADS);
    glVertex3f(x + width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y - height / 2, z + depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z + depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z - depth / 2);
    glEnd();

    // Top face
    glBegin(GL_QUADS);
    glVertex3f(x - width / 2, y + height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y + height / 2, z + depth / 2);
    glVertex3f(x - width / 2, y + height / 2, z + depth / 2);
    glEnd();

    // Bottom face
    glBegin(GL_QUADS);
    glVertex3f(x - width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y - height / 2, z - depth / 2);
    glVertex3f(x + width / 2, y - height / 2, z + depth / 2);
    glVertex3f(x - width / 2, y - height / 2, z + depth / 2);
    glEnd();

    GLfloat material_diffuse_green[] = { 0.0f, 1.0f, 0.0f, 1.0f };
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_green);

    //�������� ���'�
    glMap2f(GL_MAP2_VERTEX_3, 0, 1, 3, 4, 0, 1, 12, 4, &ctrlpoints[0][0][0]);
    glEnable(GL_MAP2_VERTEX_3);
    glMapGrid2f(30, 0.0, 1.0, 30, 0.0, 1.0);
    glEvalMesh2(GL_FILL, 0, 30, 0, 30);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (smoothShading)
        glShadeModel(GL_SMOOTH);
    else
        glShadeModel(GL_FLAT);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    


    if (isPerspective) {
        // ������������ ��������
        gluPerspective(45, 2, 2, 100);
    }
    else {
        // ������������ ��������
        glOrtho(-6, 6, -4, 4, 2, 80);
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);

    if (!isBezier) {
        GLfloat light_position[] = { 4.0f, 4.0f, 4.0f, 0.0f };
        GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

        GLfloat material_diffuse_red[] = { 1.0f, 0.0f, 0.0f, 1.0f };
        glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_red);
        //����� � ��������
        glPushMatrix();
        glTranslatef(-2, 0, 0);
        glutWireSphere(1, 20, 20);
        glPopMatrix();

        GLfloat material_diffuse_blue[] = { 0.0f, 0.0f, 1.0f, 1.0f }; // ����� ���� ��������
        glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffuse_blue);

        //����� � ������������� �������
        glPushMatrix();
        glTranslatef(2, 0, 0);
        glutSolidSphere(1, 20, 20);
        glPopMatrix();


    }
    else {

        GLfloat light_position[] = { 4.0f, 4.0f, -4.0f, 0.0f };
        GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glLightfv(GL_LIGHT0, GL_POSITION, light_position);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

        drawBezier();
    }


    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
    if (key == 'p') {
        isPerspective = !isPerspective;
        glutPostRedisplay();
    }
    else if (key == 'w') {
        eyeY += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 's') {
        eyeY -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'a') {
        eyeX -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'd') {
        eyeX += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'z') {
        eyeZ -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'x') {
        eyeZ += 0.1f;
        glutPostRedisplay();
    }

    else if (key == 'o') {
        isBezier = !isBezier;
        glutPostRedisplay();
    }

    else if (key == 'i'){
        smoothShading = !smoothShading;
        glutPostRedisplay();
    }
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w / (float)h, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Lab 3");
    glEnable(GL_DEPTH_TEST);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutMainLoop();
    return 0;
}

