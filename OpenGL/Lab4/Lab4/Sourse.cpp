//#include <glut.h>
#include<gl/glut.h> 
#include <SOIL.h>
#include <stdio.h>

#pragma warning(disable:4996)

float eyeX = 0.0f;
float eyeY = 0.0f;
float eyeZ = 5.0f;
float centerX = 0.0f;
float centerY = 0.0f;
float centerZ = 0.0f;
float upX = 0.0f;
float upY = 1.0f;
float upZ = 0.0f;

GLuint starTexture, planetTexture, backgroundTexture;

float planetAngle = 0.0f;
float planetOrbitAngle = 0.0f;
float planetOrbitRadius = 4.0f;

bool toMove = false;

float planetAng = 3.3;
float sunAng = 0.3;

GLuint loadTexture(const char* filename) {
    GLuint textureID = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_TEXTURE_REPEATS);
    if (textureID == 0) {
        printf("Error loading texture: %s\n", SOIL_last_result());
    }
    return textureID;
}


void drawBackground() {
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, backgroundTexture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    GLUquadricObj* quadric3 = gluNewQuadric();
    gluQuadricTexture(quadric3, GL_TRUE);
    gluSphere(quadric3, 10, 20, 20);

    glDisable(GL_TEXTURE_2D);
}

void display() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 2, 2, 100);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);

    glDisable(GL_LIGHTING);

    

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, starTexture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    GLUquadricObj* quadric = gluNewQuadric();
    gluQuadricTexture(quadric, GL_TRUE);
    gluSphere(quadric, 1, 20, 20);

    glDisable(GL_TEXTURE_2D);

    // ��������� ���������
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // ��������� ������� �����
    GLfloat light_position[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

    glEnable(GL_FOG);
    GLfloat fogColor[] = { 0.5f, 0.5f, 1.0f, 1.0f }; // ���� ������ (����)
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_MODE, GL_EXP); // ����� ������: ��������������
    glFogf(GL_FOG_DENSITY, 0.1f); // ������� ������

    // ���������� �������� �� �������
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, planetTexture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glPushMatrix();
    glRotatef(planetOrbitAngle, 0, 1, 0);
    glTranslatef(planetOrbitRadius, 0, 0);
    glRotatef(planetAngle, 0, 1, 0);
    GLUquadricObj* quadric2 = gluNewQuadric();
    gluQuadricTexture(quadric2, GL_TRUE);
    gluSphere(quadric2, 0.5, 20, 20);
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);

    glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y) {
    if (key == 'w') {
        eyeY += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 's') {
        eyeY -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'a') {
        eyeX -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'd') {
        eyeX += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'z') {
        eyeZ -= 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'x') {
        eyeZ += 0.1f;
        glutPostRedisplay();
    }
    else if (key == 'g'){
        toMove = !toMove;
    }
    else if (key == 'n') {
        if (planetAng == 0.3f)
        {
            planetAng = 0;
        }
        else
        {
            planetAng = 0.3f;
        }
    }
    else if (key == 'm') {
        if (sunAng == 0.3f)
        {
            sunAng = 0;
        }
        else
        {
            sunAng = 0.3f;
        }
    }
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (float)w / (float)h, 1, 100);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
}

void timer(int value) {
    if (toMove == true)
    {
        planetAngle += planetAng;
        planetOrbitAngle += sunAng;
        if (planetAngle >= 360.0f) {
            planetAngle -= 360.0f;
        }
        if (planetOrbitAngle >= 360.0f) {
            planetOrbitAngle -= 360.0f;
        }

        glutPostRedisplay();
    }

    
    glutTimerFunc(16, timer, 0);
}


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Lab 4");
    glEnable(GL_DEPTH_TEST);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, timer, 0);

    starTexture = loadTexture("starTexture.jpg");
    planetTexture = loadTexture("planetTexture.jpg");
    backgroundTexture = loadTexture("space.jpg");


    glutMainLoop();
    return 0;
}

